<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/14/2019
 * Time: 2:10 PM
 */

class SettingsController {

    function ConfigSettings() {

        $CI =& get_instance();
        foreach($CI->SiteConfig->get_all()->result() as $site_config)
        {
            $CI->config->set_item($site_config->settingName, $site_config->value);
        }

    }

}