<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/14/2019
 * Time: 2:18 PM
 */

class SiteConfig extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        return $this->db->get('settings');
    }

    public function update_config($data)
    {
        $success = true;
        foreach($data as $key=>$value)
        {
            if(!$this->save($key,$value))
            {
                $success=false;
                break;
            }
        }
        return $success;
    }

    public function save($key,$value)
    {
        $config_data=array(
            'settingName'=>$key,
            'value'=>$value
        );
        $this->db->where('settingName', $key);
        return $this->db->update('settings', $config_data);
    }
}