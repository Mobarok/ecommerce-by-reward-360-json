<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/27/2018
 * Time: 12:21 PM
 */

class Admin extends MY_Controller{

    public $today;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Admin_model');

        // load form_validation library
        $this->load->library('form_validation');

        $user_id = $this->session->userdata('user_id');
        $username = $this->session->userdata('username');
        $role = $this->session->userdata('role');

        if(empty($username)||empty($user_id)||empty($role))
        {
            redirect('/login');
        }

        $this->today = date('Y-m-d H:i:s');
    }

    //////////////////////    Dashboard    /////////////////////////


    public function dashboard()
    {
        $data['content'] = 'admin/dashboard/index';
        $data['load_script'] = 'admin/dashboard/scripts';

        
        $data['users'] =  $this->Admin_model->count_row("*", "users");

        $condition = 'parentId = 0';
        $data['categories'] =  $this->Admin_model->count_with_where("*",$condition,"categories");

        $condition = 'parentId > 0';
        $data['sub_categories'] =  $this->Admin_model->count_with_where("*",$condition,"categories");
        $data['products'] =  $this->Admin_model->count_row("*", "products");
        $data['orders'] =  $this->Admin_model->count_row("*", "orders");
        $data['shipments'] =  $this->Admin_model->count_row("*", "shipments");


        $this->load->view('admin/index', $data);
    }


    //////////////////////    Category    /////////////////////////

    public function add_category()
    {
        $data['content'] = 'admin/category/add';
        $data['load_script'] = 'admin/category/script';

        $this->load->view('admin/index', $data);
    }

    public function edit_category($id)
    {
        $categoryId = $id;

        $selector = '*';
        $table = 'categories';
        $condition = 'categoryId='.$id;
        $result =  $this->Admin_model->select_with_where($selector, $condition, $table);

        $data['content'] = 'admin/category/edit';
        $data['load_script'] = 'admin/category/script';
        $data['result'] = $result[0];

        $this->load->view('admin/index', $data);
    }

    public function categories()
    {
        $data['content'] = 'admin/category/list';
        $data['load_script'] = 'admin/category/script';

        $selector = '*';
        $table = 'categories';
        $condition = 'status=1';
        $data['categories'] = $values =  $this->Admin_model->select_with_where($selector, $condition, $table);

        $this->load->view('admin/index', $data);
    }


    //////////////////////    For Both Category / Sub category    /////////////////////////

    public function get_category($type=Null)
    {

        $selector = '*';
        $table = 'categories';

        if($type == 'sub')
            $condition = 'status=1 and parentId>0' ;
        else
            $condition = 'status=1 and parentId=0';

        $values =  $this->Admin_model->select_with_where($selector, $condition, $table);

        $data = array(
            "okResponse" => true,
            "status" => 0,
            "statusDesc" => "Found ".count($values)." categories",
            "value" => null,
            "values" => $values
        );

        header('Content-Type: application/json');

        echo json_encode($data);

    }


    public function submit_category()
    {

        $category = [];
        $category['countryId'] = $this->input->post('countryId');
        $category['parentId'] = $this->input->post('parentId');
        $category['vendorId'] = $this->input->post('vendorId');
        $category['name'] = $name = $this->input->post('name');
        $category['slug'] = $slug = $this->input->post('slug');
        $category['description'] = $this->input->post('description');
        $category['displayOrder'] = $this->input->post('displayOrder');
        $category['status'] = $this->input->post('status');
        $category['cat_featured_image'] = $this->input->post('cat_featured_image');
        $category['created'] = $this->today;

        if( empty($name)|| empty($slug) ){
            $result = false;

        }else{

            $category_id = $this->Admin_model->insert('categories', $category);

            if($category_id)
                $result = true;
        }

        header('Content-Type: application/json');

        echo json_encode($result);
    }


    public function uploadImage($folder)
    {
        sleep(4);
        if ($folder == "product"){
            $alt = $this->input->post('alt');
            $vendor = $this->input->post('vendor');
        }else{
            $alt = $vendor = "";
        }

        if($_FILES["files"]["name"] != '')
        {
            $output = [] ;
            $config["upload_path"] = base_url().'/upload/'.$folder."/";
            $config["allowed_types"] = 'gif|jpg|png|jpeg';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            for($count = 0; $count<count($_FILES["files"]["name"]); $count++)
            {
                $_FILES["file"]["name"] = $_FILES["files"]["name"][$count];
                $_FILES["file"]["type"] = $type = $_FILES["files"]["type"][$count];
                $_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"][$count];
                $_FILES["file"]["error"] = $_FILES["files"]["error"][$count];
                $_FILES["file"]["size"] = $size = $_FILES["files"]["size"][$count];

                $i_ext = explode('.', $_FILES['file']['name']);

                $target_path = date("d-m-Y")."_".uniqid().'.' .end($i_ext);
                if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/'.$folder.'/' . $target_path))
                {
                    $output[$count]['id'] = null;
                    $output[$count]['imageId'] = null;
                    $output[$count]['size'] = $size;
                    $output[$count]['type'] = end($i_ext);
                    $output[$count]['url'] = $url = base_url().'uploads/'.$folder.'/'.$target_path;
                    $output[$count]['productId'] = null;
                    $output[$count]['vendor'] = $vendor;
                    $output[$count]['alt'] = $alt;

                }
            }

        }

        if($folder=="product"){

            $data = array(
                "okResponse" => true,
                "status" => 0,
                "uploadLink" => $output,
                "value" => null,
                "values" => null
            );
        }else{
            $data = array(
                "okResponse" => true,
                "status" => 0,
                "uploadLink" => $url,
                "value" => null,
                "values" => null
            );
        }

        header('Content-Type: application/json');

        echo json_encode($data);

    }

    public function removeImage()
    {
        $url = $this->input->post('url');
        $image = explode(base_url(), $url);

        $id = $this->input->post('id');

        if($id){
            $result = $this->Admin_model->delete_function_cond('product_images', 'id="'.$id.'"');
            unlink( $image[1]);
        }

        echo "1";
    }



    public function update_category()
    {

        $categoryId = $this->input->post('categoryId');

        $selector = '*';
        $table = 'categories';
        $condition = 'categoryId='.$categoryId;
        $checkId =  $this->Admin_model->select_with_where($selector, $condition, $table);

        if($checkId){

            $category = [];
            $category['countryId'] = $this->input->post('countryId');
            $category['parentId'] = $this->input->post('parentId');
            $category['vendorId'] = $this->input->post('vendorId');
            $category['name'] = $name = $this->input->post('name');
            $category['description'] = $this->input->post('description');
            $category['displayOrder'] = $this->input->post('displayOrder');
            $category['cat_featured_image'] = $this->input->post('cat_featured_image');
            $category['status'] = $this->input->post('status');
            $category['updated'] = $this->today;

            $result = $this->Admin_model->update_function('categoryId',$categoryId, $table, $category);

            if($result)
                $result = true;
            else
                $result = false;
        }else{
            $result = false;
        }

        header('Content-Type: application/json');

        echo json_encode($result);
    }




    public function delete_category($data, $id)
    {
        $categoryId = $id;

        $selector = '*';
        $table = 'categories';
        $condition = 'categoryId='.$categoryId;
        $check =  $this->Admin_model->select_with_where($selector, $condition, $table);


        if($check)
        {
            $update['status'] = '2';
            $result = $this->Admin_model->update_function('categoryId',$categoryId, $table, $update);
        }

        if($data=='category')
            redirect('admin/categories');
        if($data=='subcategory')
            redirect('admin/subcategories');
    }


    //////////////////////    For Sub category    /////////////////////////
    ///
    ///
    public function add_subcategory()
    {
        $data['content'] = 'admin/sub-category/add';
        $data['load_script'] = 'admin/sub-category/script';

        $selector = '*';
        $table = 'categories';
        $condition = 'status=1';
        $data['categories'] =  $this->Admin_model->select_with_where($selector, $condition, $table);

        $this->load->view('admin/index', $data);
    }


    public function edit_subcategory($id)
    {
        $categoryId = $id;

        $selector = '*';
        $table = 'categories';
        $condition = 'categoryId='.$id;
        $result =  $this->Admin_model->select_with_where($selector, $condition, $table);

        $data['categories'] = $this->Admin_model->select_all('categories');
        $data['content'] = 'admin/sub-category/edit';
        $data['load_script'] = 'admin/sub-category/script';
        $data['result'] = $result[0];

        $this->load->view('admin/index', $data);
    }


    public function subcategories()
    {
        $data['content'] = 'admin/sub-category/list';
        $data['load_script'] = 'admin/sub-category/script';
        $data['sub_categories'] = $this->Admin_model->select_where_join('b.*, a.name as parentName', 'categories as a', 'categories as b', 'a.categoryId=b.parentId','b.status=1');

        $this->load->view('admin/index', $data);
    }

    //////////////////////    For Brand    /////////////////////////

    public function add_brand()
    {
        $data['content'] = 'admin/brand/add';
        $data['load_script'] = 'admin/brand/script';

        $selector = '*';
        $table = 'categories';
        $condition = 'status=1';
        $data['categories'] =  $this->Admin_model->select_with_where($selector, $condition, $table);

        $this->load->view('admin/index', $data);
    }

    public function submit_brand()
    {

        $brand = [];
        $brand['categoryId'] = $this->input->post('categoryId');
        $brand['name'] = $name = $this->input->post('name');
        $brand['slug'] = $slug = $this->input->post('slug');
        $brand['description'] = $this->input->post('description');
        $brand['status'] = $this->input->post('status');
        $brand['created'] = $this->today;

        if( empty($name)|| empty($slug) ){
            $result = false;

        }else{
            $result = $this->Admin_model->insert('brands', $brand);
        }

        header('Content-Type: application/json');

        echo json_encode($result);
    }

    public function brands()
    {
        $data['content'] = 'admin/brand/list';
        $data['brands'] = $this->Admin_model->select_where_join('a.*, b.name as categoryName', 'brands as a', 'categories as b', 'a.categoryId=b.categoryId', 'a.status=1');

        $this->load->view('admin/index', $data);
    }

    public function edit_brand($id)
    {
        $selector = '*';
        $table = 'brands';
        $condition = 'id='.$id;
        $result =  $this->Admin_model->select_with_where($selector, $condition, $table);

        $data['content'] = 'admin/brand/edit';
        $data['load_script'] = 'admin/brand/script';
        $data['result'] = $result[0];

        $selector = '*';
        $table = 'categories';
        $condition = 'status=1';
        $data['categories'] =  $this->Admin_model->select_with_where($selector, $condition, $table);

        $this->load->view('admin/index', $data);
    }

    public function update_brand()
    {
        $brandId = $this->input->post('brandId');

        $selector = '*';
        $table = 'brands';
        $condition = 'id='.$brandId;
        $checkId =  $this->Admin_model->select_with_where($selector, $condition, $table);

        if($checkId){

            $brand = [];
            $brand['categoryId'] = $this->input->post('categoryId');
            $brand['name'] = $name = $this->input->post('name');
            $brand['description'] = $this->input->post('description');
            $brand['status'] = $this->input->post('status');
            $brand['updated'] = $this->today;

            $result = $this->Admin_model->update_function('id',$brandId, $table, $brand);

            if($result)
                $result = true;
            else
                $result = false;
        }else{
            $result = false;
        }

        header('Content-Type: application/json');

        echo json_encode($result);
    }

    public function delete_brand($id)
    {
        $selector = '*';
        $table = 'brands';
        $condition = 'id='.$id;
        $check =  $this->Admin_model->select_with_where($selector, $condition, $table);


        if($check)
        {
            $update['status'] = '2';
            $result = $this->Admin_model->update_function('id',$id, $table, $update);
        }

        redirect('admin/brands');

    }

    ///////////////////// Products /////////////////////////////

    public function add_product()
    {
        $data['content'] = "product/add";
        $data['load_script'] = "product/script";
        $selector = '*';
        $condition = 'status=1';
        $data['categories'] =  $this->Admin_model->select_with_where($selector, $condition, 'categories');
        $data['brands'] =  $this->Admin_model->select_with_where($selector, $condition, 'brands');

        $this->load->view('admin/index', $data);
    }


    function product_validation()
    {

        // basic required field
        $this->form_validation->set_rules('productId', 'product Id', 'required');
        $this->form_validation->set_rules('countryId', 'Country Id', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');
        $this->form_validation->set_rules('shortDescription', 'short description', 'required');
        $this->form_validation->set_rules('productCategories', 'Product Categories', 'required');
        $this->form_validation->set_rules('sku', 'Product Sku', 'required');
        $this->form_validation->set_rules('productId', 'Product Id', 'required');
    }

    public function create_product()
    {

        $product = [];
        $product['productId'] = $this->input->post('productId');
        $product['countryId'] = $this->input->post('countryId');
        $product['name'] = $this->input->post('name');
        $product['description'] = $this->input->post('description');
        $product['specifications'] = $this->input->post('specifications');
        $categories = $this->input->post('productCategories');

        $count = count($categories);
        $cat = '[';
        foreach ( $categories as $key => $category){
             $cat = $cat.$category;
            ($count != $key+1) ? $cat = $cat."," : "";
        }

        $product['productCategories'] = $cat."]";
        $product['similarProducts'] = $this->input->post('similarProducts');
        $product['active'] = $this->input->post('active') == "true" ? 1 : 0;
        $product['brand'] = $this->input->post('brand');
        $product['stockCount'] = $this->input->post('stockCount');
        $product['sku'] = $this->input->post('stockCount');
        $product['validFrom'] = $this->input->post('validForm');
        $product['validTill'] = $this->input->post('validTill');

        $customerCategory = $this->input->post('customerCategory');
        $count = count($customerCategory);
        $cusCat = '[';
        foreach ( $customerCategory as $key => $value){
            $cusCat = $cusCat.$value;
            ($count != $key+1) ? $cusCat = $cusCat."," : "";
        }

        $product['customerCategory'] = $cusCat."]";

        $customerFlags = $this->input->post('customerFlags');
        $count = count($customerFlags);
        $cusFlags = '[';
        foreach ( $customerFlags as $key => $value){
            $cusFlags = $cusFlags.$value;
            ($count != $key+1) ? $cusFlags = $cusFlags."," : "";
        }

        $product['customerFlags'] = $cusFlags."]";
        $product['productType'] = $this->input->post('productType');
        $product['redemptionType'] = $this->input->post('redemptionType');
        $product['voucherId'] = $this->input->post('voucherId');
        $product['failureTempleteId'] = $this->input->post('failureTempleteId');
        $product['successTempleteId'] = $this->input->post('successTempleteId');

        $price = $this->input->post('productPrice');

        $product['price_mrp'] = $price['mrp'];
        $product['client_price'] = $price['clientPrice'];
        $product['discount'] = $price['discount'];
        $product['minimumPoints'] = $price['minimumPoints'];
        $product['minimumCash'] = $price['minimumCash'];
        $product['ratio'] = $price['ratio'];
        $product['actualPoints'] = $price['actualPoints'];
        $product['basePoints'] = $price['basePoints'];
        $product['currencyCode'] = $price['currencyCode'];


        $product['shortDescription'] = $this->input->post('shortDescription');
        $product['status'] = $this->input->post('status');
        $product['vendorProductStatus'] = $this->input->post('vendorProductStatus');
        $product['similarProductsByVendor'] = $this->input->post('similarProductsByVendor');
        $product['partnerId'] = $this->input->post('partnerId');
        $product['vendorSku'] = $this->input->post('vendorSku');
        $product['subCustomerCategory'] = $this->input->post('subCustomerCategory');
        $product['vendorCategory'] = $this->input->post('vendorCategory');
        $product['created'] = $this->today;

        $this->db->trans_start();

        $result = $this->Admin_model->insert_ret('products', $product);

        $images = json_decode($this->input->post('productImages'));

//        print_r($images);
        $count = count($images);
        $productImage = [];
        for($i=0; $i<$count; $i++)
        {
            $productImage['url'] = $images[$i]->url;
            $productImage['type'] = $images[$i]->type;
            $productImage['size'] = $images[$i]->size;
            $productImage['alt'] = $images[$i]->alt;
            $productImage['productId'] = $result;
            $productImage['imageId'] = $i+1;
            $productImage['displayOrderId'] = $i+1;
            $productImage['created'] = $this->today;
            $productImage['vendor'] = $images[$i]->vendor;

            $this->Admin_model->insert('product_images', $productImage);
        }


        $this->db->trans_complete();

        if( $result>0 ){
            $statusMsg = 0; // O (Zero) for Success as per API
            $msg = " You have successfully added a new product";
        }else{
            $statusMsg = 1; // O (Zero) for failure as Per API
            $msg = "Unable to Add Product";
        }

        $data = array(
            "okResponse" => true,
            "status" => $statusMsg,
            "statusDesc" => $msg,
            "value" => null,
            "values" => null
        );

        header('Content-Type: application/json');

        echo json_encode($data);
    }


    public function products()
    {
        $data['content'] = "product/list";
        $data['load_script'] = "product/script";
        $this->load->view('admin/index', $data);
    }

    public function getProducts()
    {

        $selector = '*';
        $table = 'products';
        $condition ='status=0 AND active=1';
        $values =  $this->Admin_model->select_with_where($selector, $condition, $table);

        $data = array(
            "okResponse" => true,
            "status" => 0,
            "statusDesc" => "Found ".count($values)." products",
            "value" => null,
            "values" => $values
        );

        header('Content-Type: application/json');

        echo json_encode($data);
    }

    public function edit_product($id)
    {
        $data['content'] = "product/edit";
        $data['load_script'] = "product/edit_product_script";

        $result =  $this->Admin_model->select_with_where('id', "id=".$id, 'products');

        if(!$result[0]['id']){
            redirect('admin/products');
        }

        $this->session->set_userdata('editProductId', $id);

        $selector = '*';
        $condition = 'status=1';
        $data['categories'] =  $this->Admin_model->select_with_where($selector, $condition, 'categories');
        $data['brands'] =  $this->Admin_model->select_with_where($selector, $condition, 'brands');

        $this->load->view('admin/index', $data);
    }

    public function getProductById($id)
    {

        $result = $this->Admin_model->select_with_where('*', 'products.id='.$id, 'products');
        $values = $result = $result[0];
        $product_images = $this->Admin_model->select_with_where('*', 'product_images.productId='.$id, 'product_images');

        $values['productImages'] = [];
        foreach ( $product_images as $key => $product_image)
        {
            $arr = [];
            $arr['id'] = $product_image['id'];
            $arr['imageId'] = $product_image['imageId'];
            $arr['url'] = $product_image['url'];
            $arr['size'] = $product_image['size'];
            $arr['type'] = $product_image['type'];
            $arr['productId'] = $product_image['productId'];
            $arr['vendor'] = $product_image['vendor'];
            $arr['alt'] = $product_image['alt'];

            $values['productImages'][$key] = $arr;
        }

        $price = [];
        $price['mrp'] = $result['price_mrp'];
        $price['clientPrice'] = $result['client_price'];
        $price['currencyCode'] = $result['currencyCode'];
        $price['discount'] = $result['discount'];
        $price['minimumPoints'] = $result['minimumPoints'];
        $price['minimumCash'] = $result['minimumCash'];
        $price['basePoints'] = $result['basePoints'];
        $price['actualPoints'] = $result['actualPoints'];
        $price['ratio'] = $result['ratio'];
        $price['productId'] = $result['id'];
        $price['vendor'] = $result['partnerId'];

        $values['productPrice']  = $price;
//        $values = $this->Admin_model
//            ->select_where_left_join(
//                'products.*, product_images.*, product_images.id as productImageId',
//                'products',
//                'product_images',
//                'products.id = product_images.productId',
//                'products.id='.$id
//            );

        $data = array(
            "okResponse" => true,
            "status" => 0,
            "statusDesc" => "Found : Product",
            "value" => null,
            "values" => json_encode($values)
        );

        header('Content-Type: application/json');

        echo json_encode($data);
    }


    public function update_product()
    {

        $product = [];
        $productId = $this->input->post('productId');
        $vendor = $this->input->post('partnerId');
        $updateProductId = $this->input->post('updateProductId');

        $product['countryId'] = $this->input->post('countryId');
        $product['name'] = $this->input->post('name');
        $product['description'] = $this->input->post('description');
        $product['specifications'] = $this->input->post('specifications');
        $categories = $this->input->post('productCategories');

        $count = count($categories);
        $cat = '[';
        foreach ( $categories as $key => $category){
            $cat = $cat.$category;
            ($count != $key+1) ? $cat = $cat."," : "";
        }

        $product['productCategories'] = $cat."]";
        $product['similarProducts'] = $this->input->post('similarProducts');
        $product['active'] = $this->input->post('active') == "true" ? 1 : 0;
        $product['brand'] = $this->input->post('brand');
        $product['stockCount'] = $this->input->post('stockCount');
        $product['sku'] = $this->input->post('stockCount');
        $product['validFrom'] = $this->input->post('validForm');
        $product['validTill'] = $this->input->post('validTill');

        $customerCategory = $this->input->post('customerCategory');
        $count = count($customerCategory);
        $cusCat = '[';
        foreach ( $customerCategory as $key => $value){
            $cusCat = $cusCat.$value;
            ($count != $key+1) ? $cusCat = $cusCat."," : "";
        }

        $product['customerCategory'] = $cusCat."]";

        $customerFlags = $this->input->post('customerFlags');
        $count = count($customerFlags);
        $cusFlags = '[';
        foreach ( $customerFlags as $key => $value){
            $cusFlags = $cusFlags.$value;
            ($count != $key+1) ? $cusFlags = $cusFlags."," : "";
        }

        $product['customerFlags'] = $cusFlags."]";
        $product['productType'] = $this->input->post('productType');
        $product['redemptionType'] = $this->input->post('redemptionType');
        $product['voucherId'] = $this->input->post('voucherId');
        $product['failureTempleteId'] = $this->input->post('failureTempleteId');
        $product['successTempleteId'] = $this->input->post('successTempleteId');

        $price = $this->input->post('productPrice');

        $product['price_mrp'] = $price['mrp'];
        $product['client_price'] = $price['clientPrice'];
        $product['discount'] = $price['discount'];
        $product['minimumPoints'] = $price['minimumPoints'];
        $product['minimumCash'] = $price['minimumCash'];
        $product['ratio'] = $price['ratio'];
        $product['actualPoints'] = $price['actualPoints'];
        $product['basePoints'] = $price['basePoints'];
        $product['currencyCode'] = $price['currencyCode'];


        $product['shortDescription'] = $this->input->post('shortDescription');
        $product['status'] = $this->input->post('status');
        $product['vendorProductStatus'] = $this->input->post('vendorProductStatus');
        $product['similarProductsByVendor'] = $this->input->post('similarProductsByVendor');
        $product['partnerId'] = $this->input->post('partnerId');
        $product['vendorSku'] = $this->input->post('vendorSku');
        $product['subCustomerCategory'] = $this->input->post('subCustomerCategory');
        $product['vendorCategory'] = $this->input->post('vendorCategory');
        $product['updated'] = $this->today;

//        $condition = array('id' => $updateProductId,'productId' => $productId, 'partnerId' => $vendor);
        $result = $this->Admin_model
                        ->update_function( 'id', $updateProductId, 'products', $product);

        $images = json_decode($this->input->post('productImages'));

        $count = count($images);
        $productImage = [];
        for($i=0; $i<$count; $i++)
        {
            $productImage['url'] = $images[$i]->url;
            $productImage['type'] = $images[$i]->type;
            $productImage['size'] = $images[$i]->size;
            $productImage['alt'] = $images[$i]->alt;
            $productImage['productId'] = $updateProductId;
            $productImage['imageId'] = $i+1;
            $productImage['displayOrderId'] = $i+1;
            $productImage['created'] = $this->today;
            $productImage['vendor'] = $images[$i]->vendor;

            if( $images[$i]->url != null )
                $this->Admin_model->insert('product_images', $productImage);
        }


        if($result){
            $statusMsg = 0; // O (Zero) for Success as per API
            $msg = " Product Updated Successfully";
        }else{
            $statusMsg = 1; // O (Zero) for failure as Per API
            $msg = "Unable to update Product";
        }

        $data = array(
            "okResponse" => true,
            "status" => $statusMsg,
            "statusDesc" => $msg,
            "value" => null,
            "values" => null
        );

        header('Content-Type: application/json');

        echo json_encode($data);
    }

    ///////////////////// Users  /////////////////////////////

    public function users()
    {
        $data['content'] = "users/list";
        $table = 'users';
        $data['users'] = $values =  $this->Admin_model->select_all($table);

        $this->load->view('admin/index', $data);
    }

    public function add_user()
    {
        $data['content'] = "users/add";
        $this->load->view('admin/index', $data);
    }

    public function create_user()
    {

        $check_unique_email = $this->check_email_unique();
        $check_unique_username = $this->check_username_unique();

        if( $check_unique_email==1 || $check_unique_username==1 ) {

            if($check_unique_email){
                $message = array(
                    'type' => 'danger',
                    'text' => 'Email already Exist!',
                );
            }else{
                $message = array(
                    'type' => 'danger',
                    'text' => 'Username already Exist!',
                );
            }

            $this->session->set_flashdata('message', $message);

            redirect("admin/add_user");
        }

        $users = [];
        $users['username'] = $this->input->post('username');
        $users['email'] = $this->input->post('email');
        $users['full_name'] = $this->input->post('full_name');
        $users['role'] = $this->input->post('role');
        $users['phone'] = $this->input->post('phone');
        $users['address'] = $this->input->post('address');
        $users['image'] = '';

        if($users['role']==1)
        {
            $password = 'admin';
        }elseif ($users['role']==2)
        {
            $password = 'subadmin';
        }else{
            $password = 'user';
        }

        $users['password'] = $this->encrypt_decrypt($password);

        if(!empty($_FILES['image']['tmp_name']))
        {
            $i_ext = explode('.', $_FILES['image']['name']);
            $target_path = date("d-m-Y")."_".uniqid().'.' .end($i_ext);

            $size = getimagesize($_FILES['image']['tmp_name']);

            if (move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/users/' . $target_path))
            {
                $users['image'] = base_url().'uploads/users/'.$target_path;
            }


            if ($size[0] == 100 || $size[1] == 100) {

            }
            else {
                $imageWidth = 100; //Contains the Width of the Image

                $imageHeight = 100;

                $this->resize($imageWidth, $imageHeight, "uploads/users/" . $target_path, "uploads/users/" . $target_path);
            }
        }

        $result = $this->Admin_model->insert('users', $users);

        $message = array(
            'type' => 'success',
            'text' => 'You have successfully added a new user',
        );

        $this->session->set_flashdata('message', $message);

        redirect("admin/users");
    }


    public function edit_user($id)
    {
        $user_id = $id;

        $selector = '*';
        $table = 'users';
        $condition = 'id='.$user_id;
        $result =  $this->Admin_model->select_with_where($selector, $condition, $table);

        //print_r($result);
        
        $data['content'] = "users/edit";
        $data['users'] = $result[0];
        $this->load->view('admin/index', $data);
    }
    
    public function update_user($id)
    {
        $user_id = $id;

        $selector = '*';
        $table = 'users';
        $condition = 'id='.$user_id;
        $checkId =  $this->Admin_model->select_with_where($selector, $condition, $table);

        $user_image = $checkId[0]['image'];


        $users = [];
        $users['full_name']  = $this->input->post('full_name');
        $users['username']= $username = $this->input->post('username');
        $users['email'] = $email = $this->input->post('email');

        if($checkId)
        {

            if( ($email != $checkId[0]['email']) || ($username != $checkId[0]['username']))
            {
                $message = array(
                    'type' => 'danger',
                    'text' => 'You can\'t change Email or Username!',
                );

                $this->session->set_flashdata('message', $message);

                redirect($_SERVER['HTTP_REFERER']);
            }


            $users['phone'] = $this->input->post('phone');
            $users['role'] = $this->input->post('role');
            $users['address'] = $this->input->post('address');

            if($users['role']==1)
            {
                $password = 'admin';
            }elseif ($users['role']==2)
            {
                $password = 'subadmin';
            }else{
                $password = 'user';
            }

            $users['password'] = $this->encrypt_decrypt($password);


            if(!empty($_FILES['image']['tmp_name']))
            {
                $i_ext = explode('.', $_FILES['image']['name']);
                $target_path = date("d-m-Y")."_".uniqid().'.' .end($i_ext);

                $size = getimagesize($_FILES['image']['tmp_name']);

                if (move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/users/' . $target_path))
                {
                    if($user_image){

                        $image = explode(base_url(), $user_image);
                        unlink( $image[1]);
                    }

                    $users['image'] = base_url().'uploads/users/'.$target_path;
                }


                if ($size[0] == 100 || $size[1] == 100) {

                }
                else {
                    $imageWidth = 100; //Contains the Width of the Image

                    $imageHeight = 100;

                    $this->resize($imageWidth, $imageHeight, "uploads/users/" . $target_path, "uploads/users/" . $target_path);
                }
            }

            //print_r($users);
            $result = $this->Admin_model->update_function('id',$user_id, $table, $users);
        }

        if($result)
        {
            $message = array(
                'type' => 'success',
                'text' => 'You have successfully updated a user info',
            );

            $this->session->set_flashdata('message', $message);
        }

        redirect("admin/users");
    }

    public function delete_users($id)
    {
        $user_id = $id;

        $selector = '*';
        $table = 'users';
        $condition = 'id='.$user_id;
        $check =  $this->Admin_model->select_with_where($selector, $condition, $table);


        if($check)
        {
            $update['status'] = '2';
            $result = $this->Admin_model->update_function('id',$user_id, $table, $update);
        }

        if($result)
        {
            $this->session->set_flashdata('msg', 'Data Deleted Successfully');
            redirect("admin/users");
        }
    }

    public function user_status($status)
    {
        $data['content'] = "users/status";
        $data['status'] = $status;

        $selector = '*';
        $table = 'users';

        if($status=="active")
            $condition = 'status=1';
        elseif($status=="inactive")
            $condition = 'status=2';
        else
            redirect('admin/users');

        $data['users'] = $values =  $this->Admin_model->select_with_where($selector, $condition, $table);
        
        $this->load->view('admin/index', $data);
    }

    public function update_status($id)
    {
        $user_id = $id;

        $selector = '*';
        $table = 'users';
        $condition = 'id='.$user_id;
        $check =  $this->Admin_model->select_with_where($selector, $condition, $table);


        if($check)
        {
            $update['status'] = '1';
            $result = $this->Admin_model->update_function('id',$user_id, $table, $update);
        }

        if($result)
        {
            $this->session->set_flashdata('msg', 'Data Updated Successfully');
            redirect("admin/user_status/active");
        }
    }

    ///////////////////// Users Profile /////////////////////////////

    public function profile()
    {
        $data['content'] = "profile/index";
        $user_id = $this->session->userdata('user_id');
        $condition = 'id = ' . $user_id;
        $result = $this->Admin_model->select_with_where('*',$condition,'users');
        $data['user'] = $result[0];
        //print_r($data['user']['username']);
        if($data['user']['role'] = 1){
            $data['user']['role'] = 'Admin';
        }elseif($data['user']['role'] = 2){
            $data['user']['role'] = 'Sub Admin';
        }else{
            $data['user']['role'] = 'User';
        }
       $this->load->view('admin/index', $data);
    }

    public function edit_profile()
    {
        $username = $this->session->userdata('username');

        $selector = '*';
        $table = 'users';
        $condition = 'username ="'.$username.'"';
        $result =  $this->Admin_model->select_with_where($selector, $condition, $table);

        //print_r($result);
        
        $data['content'] = "profile/edit_profile";
        $data['user'] = $result[0];

        if($data['user']['role'] = 1){
            $data['user']['role'] = 'Admin';
        }elseif($data['user']['role'] = 2){
            $data['user']['role'] = 'Sub Admin';
        }else{
            $data['user']['role'] = 'User';
        }
        $this->load->view('admin/index', $data);
    }

    public function update_profile()
    {
        $username = $this->session->userdata('username');

        $selector = '*';
        $table = 'users';
        $condition = 'username ="'.$username.'"';
        $checkId =  $this->Admin_model->select_with_where($selector, $condition, $table);

        $user_id = $checkId[0]['id'];
        $user_image = $checkId[0]['image'];

        if($checkId)
        {
            $users = [];
            $users['full_name'] = $this->input->post('full_name');
            $users['phone'] = $this->input->post('phone');
            $users['address'] = $this->input->post('address');

            if(!empty($_FILES['image']['tmp_name']))
            {

                $i_ext = explode('.', $_FILES['image']['name']);
                $target_path = date("d-m-Y")."_".uniqid().'.' .end($i_ext);

                $size = getimagesize($_FILES['image']['tmp_name']);

                if (move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/users/' . $target_path))
                {
                    if($user_image){
                        $image = explode(base_url(), $user_image);
                        //print_r($image);
                        //exit;
                        unlink( $image[1]);
                    }

                    $users['image'] = base_url().'uploads/users/'.$target_path;

                    $this->session->set_userdata('profileImage', $users['image']);
                }


                if ($size[0] == 100 || $size[1] == 100) {

                }
                else {
                    $imageWidth = 100; //Contains the Width of the Image

                    $imageHeight = 100;

                    $this->resize($imageWidth, $imageHeight, "uploads/users/" . $target_path, "uploads/users/" . $target_path);
                }


            }else{
                $this->session->set_userdata('profileImage', $checkId[0]['image']);
            }
           

            //print_r($users);
            $result = $this->Admin_model->update_function('id',$user_id, $table, $users);
        }


        if($result)
        {
            $message = array(
                'type' => 'success',
                'text' => 'Profile Update Successfully',
            );

            $this->session->set_flashdata('message', $message);

        }

        redirect('admin/profile');
    }

    public function change_password()
    {
        $data['content'] = "profile/change_password";
        $this->load->view('admin/index', $data);
    }

    public function update_password()
    {
        $user_id = $this->session->userdata('user_id');
        $password =  $this->encrypt_decrypt($this->input->post('old_password'));
        
        $selector = '*';
        $table = 'users';
        $condition = 'id='.$user_id;
        $values = $this->Admin_model->select_with_where($selector, $condition, $table);
        $check = $values[0];

        //print_r($check);

        if($check && $check['password'] == $password)
        {
            
            $update['password'] =  $this->encrypt_decrypt($this->input->post('new_password'));
            $result = $this->Admin_model->update_function('id',$user_id, $table, $update);
        }

        if($result){
            $this->session->set_flashdata('msg', 'Password Changed Successfully');
            redirect('admin/change_password');
        }
        
    }

    ///////////////////// Orders /////////////////////////////


    public function orders()
    {
        $data['content'] = "order/list";
        $data['load_script'] = "order/script";
        $data['load_style'] = "order/style";
        $this->load->view('admin/index', $data);
    }

    public function order_history()
    {
        $data['content'] = "order/history";
        $data['load_script'] = "order/script";
        $data['load_style'] = "order/style";
        $this->load->view('admin/index', $data);
    }


///////////////////// Shipment Manage /////////////////////////////

    public function shipment_schedule()
    {
        $data['content'] = "shipment/list";
        $data['load_script'] = "shipment/script";
        $data['load_style'] = "shipment/style";

         $data['flag']=$this->uri->segment(3);

        $this->load->view('admin/index', $data);
    }


    public function shipment_history()
    {
        $data['content'] = "shipment/history";
        $data['load_script'] = "shipment/script";
        $data['load_style'] = "shipment/style";

        $data['flag']=$this->uri->segment(3);

        $this->load->view('admin/index', $data);
    }

    public function shipment_details()
    {
        $data['content'] = "shipment/details";

        $this->load->view('admin/index', $data);
    }






///////////////////// Stock Manage /////////////////////////////

    public function edit_stock($id, $qty)
    {
        $data['productId'] = $id;
        $data['qty'] = $qty;
        $data['content'] = "stock/update";
        $data['load_script'] = "stock/script";

        $data['all_product'] = $this->Admin_model->select_all('products');

        $this->load->view('admin/index', $data);
    }

    public function update_stock($id, $qty)
    {
        $data['stockCount'] = $qty;
        $result = $this->Admin_model->update_function('id',$id, 'products', $data);

        if($result)
            $result = true;
        else
            $result = false;


        header('Content-Type: application/json');

        echo json_encode($result);
    }


    public function stocks()
    {
        $data['content'] = "stock/list";
        $data['load_script'] = "stock/script";
        $this->load->view('admin/index', $data);
    }


    public function getStocks()
    {
        $values = $this->Admin_model
            ->select_condition_column_all('id as productId,name, partnerId as vendor, stockCount','products');


        $map = [];
        foreach ($values as $value)
        {
            $name = $value['productId'].":". $value['name'].":".$value['vendor'];
//            $name = $value['productId'].":".$value['vendor'];
            $map[$name] = $value['stockCount'];
        }


        $data = array(
            "okResponse" => true,
            "status" => 0,
            "statusDesc" => null,
            "value" => json_encode($map)
        );

        header('Content-Type: application/json');


        echo json_encode($data);

    }

    ///////////////////// General Function /////////////////////////////

    public function resize($width, $height, $source, $destination)
    {
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = $source;
        $config['overwrite'] = TRUE;
        $config['quality'] = "100%";
        $config['maintain_ratio'] = FALSE;
        $config['master_dim'] = 'height';
        $config['height'] = $height;
        $config['width'] = $width;
        $config['new_image'] = $destination; //you should have write permission here..
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    public function resize2($height,$width)
    {
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = "uploads/watermark.png";
        $config['overwrite'] = TRUE;
        $image_config['quality'] = "100%";
        $image_config['maintain_ratio'] = FALSE;
        $config['height'] = $height;
        $config['width'] = $width;
        $config['new_image'] = "uploads/product/";//you should have write permission here..
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }



    private function set_upload_options($file_name,$folder_name)
    {
        //upload an image options
        $url=base_url();

        $config = array();
        $config['file_name'] = $file_name;
        $config['upload_path'] = 'uploads/'.$folder_name;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']      = '0';
        $config['overwrite']     = TRUE;

        return $config;
    }




    public function watermark_image($imageWidth,$imageHeight)
    {
        $file = 'uploads/watermark.png';
        $newfile ='uploads/watermark.png';

        if (!copy($file, $newfile)) {
            // echo "failed to copy $file...\n";
        }else{
            // echo "copied $file into $newfile\n";
        }

        //shell_exec("cp -r ".$file." ".$newfile);
        //echo $imageWidth.' '.$imageHeight;
        //die();
        $this->resize2($imageWidth,$imageHeight);


    }




    public function overlay($path)
    {
        $config['image_library']    = 'gd2';
        $config['source_image']     = 'uploads/product/'.$path;
        $config['wm_type']          = 'overlay';
        $config['wm_overlay_path']  = 'uploads/watermark.png'; //the overlay image
        $config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'center';
        $config['wm_padding'] = '0';
        // $config['wm_x_transp'] = '200';
        //$config['wm_y_transp'] = '20';
        $config['wm_opacity'] = '50';

        $this->image_lib->initialize($config);


        if (!$this->image_lib->watermark()) {
            echo $this->image_lib->display_errors();
        }
    }

    /////////////////////////// Purchase ////////////////////////////

    public function purchases()
    {
        $data['content'] = 'purchase/list';
        $data['load_script'] = 'purchase/script';
        $selector = '*';
        $table = 'purchases';
        $condition = 'status=0';

        $data['flag']=$this->uri->segment(3);


        $data['purchases'] = $values =  $this->Admin_model->select_join('a.*,b.*','purchases as a','purchase_details as b','a.purchase_id=b.purchase_id');        
        
        
        
        $this->load->view('admin/index', $data);
    }


    public function add_purchase()
    {
        $data['content'] = "purchase/add";
        $data['load_script'] = "purchase/script";
        $selector = '*';
        $condition = 'status=1';
        $data['categories'] =  $this->Admin_model->select_with_where($selector, $condition, 'categories');
        $data['brands'] =  $this->Admin_model->select_with_where($selector, $condition, 'brands');
        $this->load->view('admin/index', $data);
    }

    public function create_purchase()
    {
        $purchase = [];
        $total = 0;

            $purchase['vendor'] = $this->input->post('vendor');
            $purchase['remark'] = $this->input->post('remark');
//            $purchase['due'] = $this->input->post('due');
            $purchase['payment'] = $payment = $this->input->post('payment');
            $purchase['status'] = $this->input->post('status');
            $purchase['created_at'] = $this->today;


            $this->db->trans_start();
            $result = $this->Admin_model->insert_ret('purchases', $purchase);

            if($result > 0){
                $purchaseItems = $this->input->post('product_name');


                for( $i=0; $i<count($purchaseItems); $i++)
                {
                    $purchaseDetails = [];
                    $purchaseDetails['purchase_id'] = $result;
                    $purchaseDetails['product_name'] = $this->input->post('product_name')[$i];
                    $purchaseDetails['product_id'] = $this->input->post('product_id')[$i];
                    $purchaseDetails['product_price'] = $price = $this->input->post('product_price')[$i];
                    $purchaseDetails['description'] = $this->input->post('description')[$i];
                    $purchaseDetails['quantity'] = $qty = $this->input->post('quantity')[$i];
                    $purchaseDetails['product_category_id'] = $this->input->post('product_category')[$i];
                    $purchaseDetails['total']  = $price*$qty;

                    $total += $purchaseDetails['total'];
                    $purchaseDetails['brand'] = $this->input->post('brand')[$i];
                    $purchaseDetails['created_at'] = $this->today;

                    $rs = $this->Admin_model->insert('purchase_details', $purchaseDetails);
                }

                $data['total_price'] = $total;
                $data['due'] = $total-$payment;

                $this->Admin_model->update_function('purchase_id', $result, 'purchases', $data);

            }


            $this->db->trans_complete();

            if($result)
            {
                $message = array(
                    'type' => 'success',
                    'text' => 'You have successfully added a new purchase',
                );

                $this->session->set_flashdata('message', $message);
            }

        redirect('admin/purchases');

    }

    public function purchase_details($id)
    {
        $purchase_id = $id;

        $selector = '*';
        $table = 'purchases';
        $condition = 'purchase_id="'.$purchase_id.'"';
        $result =  $this->Admin_model->select_with_where($selector, $condition ,$table);
//        $purchase_details =  $this->Admin_model->select_where_join($selector, $condition ,'purchase_details');
        $purchase_details =  $this->Admin_model->select_where_join('a.*, b.name as categoryName', 'purchase_details as a', 'categories as b', 'a.product_category_id=b.categoryId', $condition);

        $data['content'] = 'purchase/details';
        $data['result'] = $result[0];
        $data['purchase_details'] = $purchase_details;

        $this->load->view('admin/index', $data);
    }

   
    public function edit_purchase($id)
    {
        $purchase_id = $id;

        $selector = '*';
        $table = 'purchases';
        $condition = 'purchase_id="'.$purchase_id.'"';
        $result =  $this->Admin_model->select_with_where($selector, $condition ,$table);
        $purchase_details =  $this->Admin_model->select_with_where($selector, $condition ,'purchase_details');

        //print_r($result);
        $condition = 'status=1';
        $data['categories'] =  $this->Admin_model->select_with_where($selector, $condition, 'categories');
        $data['brands'] =  $this->Admin_model->select_with_where($selector, $condition, 'brands');
        $data['content'] = 'purchase/edit';
        $data['load_script'] = 'purchase/script';
        $data['result'] = $result[0];
        $data['purchase_details'] = $purchase_details;

        $this->load->view('admin/index', $data);
    }

    public function update_purchase()
    {

        $purchase_id = $this->input->post('purchase_id');

        $selector = '*';
        $table = 'purchases';
        $condition = 'purchase_id='.$purchase_id;
        $checkId =  $this->Admin_model->select_with_where($selector, $condition, $table);


        if($checkId)
        {

            $purchase = [];
            $total = 0;

            $purchase['vendor'] = $this->input->post('vendor');
            $purchase['remark'] = $this->input->post('remark');
            $purchase['total_price'] = $this->input->post('total_price');
//            $purchase['due'] = $this->input->post('due');
            $purchase['payment'] = $payment = $this->input->post('payment');
            $purchase['status'] = $this->input->post('status');


            $this->db->trans_start();
            //print_r($purchase);
            $result = $this->Admin_model->update_function('purchase_id',$purchase_id, 'purchases', $purchase);


            if($result > 0){
                $purchaseItems = $this->input->post('product_name');


                for( $i=0; $i<count($purchaseItems); $i++)
                {
                    $purchaseDetails = [];
                    $purchase_details_id = $this->input->post('purchase_details_id')[$i];
                    $purchaseDetails['product_name'] = $this->input->post('product_name')[$i];
                    $purchaseDetails['product_id'] = $this->input->post('product_id')[$i];
                    $purchaseDetails['product_price'] = $price = $this->input->post('product_price')[$i];
                    $purchaseDetails['description'] = $this->input->post('description')[$i];
                    $purchaseDetails['quantity'] = $qty = $this->input->post('quantity')[$i];
                    $purchaseDetails['product_category_id'] = $this->input->post('product_category')[$i];
                    $purchaseDetails['total']  = $price*$qty;

                    $total += $purchaseDetails['total'];
                    $purchaseDetails['brand'] = $this->input->post('brand')[$i];
                    $purchaseDetails['updated_at'] = $this->today;

                    if($purchase_details_id){
                    $rs = $this->Admin_model->update_function('purchase_details_id',$purchase_details_id, 'purchase_details', $purchaseDetails);
                    }else{
                        $purchaseDetails['created_at'] = $this->today;
                        $purchaseDetails['purchase_id'] = $purchase_id;

                        $rs = $this->Admin_model->insert('purchase_details', $purchaseDetails);
                    }

                }

                $data['total_price'] = $total;
                $data['due'] = $total-$payment;

                $this->Admin_model->update_function('purchase_id', $purchase_id, 'purchases', $data);

            }

            $this->db->trans_complete();
        }

        if($result)
        {
            $message = array(
                'type' => 'success',
                'text' => 'You have successfully Updated a purchase',
            );

            $this->session->set_flashdata('message', $message);
        }

        redirect('admin/purchases');
    }

    public function delete_purchase($id)
    {
        $purchase_id = $id;

        $selector = '*';
        $table = 'purchases';
        $condition = 'purchase_id='.$purchase_id;
        $check =  $this->Admin_model->select_with_where($selector, $condition, $table);


        if($check)
        {
            $update['status'] = '2';
            $result = $this->Admin_model->update_function('purchase_id',$purchase_id, $table, $update);
        }

        if($result)
        {
            $this->session->set_flashdata('msg', 'Data Deleted Successfully');
            redirect("admin/purchases");
        }
    }




    ///

    public function check_email_unique()
    {
        $email= $this->input->post('email');
        $data= $this->Admin_model->check_data('email', $email, 'users');

        if($data>0)
            return 1;
        else
          return 0;

    }


    public function check_username_unique()
    {
        $username= $this->input->post('username');
        $data= $this->Admin_model->check_data('username', $username, 'users');
        if($data>0)
        {
            return 1;
        }else{
            return 0;
        }

    }

    // Settings System

    public function settings()
    {

        $result = $this->Admin_model->Select_all('settings');

        if(count($result)==0)
        {

            $data = [];
            $data[0]['settingName'] = 'siteName';
            $data[1]['settingName'] = 'address';
            $data[2]['settingName'] = 'logo';
            $data[3]['settingName'] = 'favicon';
            $data[4]['settingName'] = 'created_at';
            $data[4]['value'] = $this->today;
            $data[5]['settingName'] = 'updated_at';
            $data[5]['value'] = $this->today;

            for ($i=0; $i<count($data); $i++) {
                $this->Admin_model->insert('settings', $data[$i]);
            }
        }

        $data = [];
        $data['content'] = 'admin/settings/index';
        $this->load->view('admin/index', $data);
    }


    public function update_settings()
    {

        $site_name = $this->input->post('site_name');
        $address = $this->input->post('address');

        if(!empty($site_name))
        {

          $data['settingName'] = "siteName";
          $data['value'] = $site_name;
          $condition = 'settingName = "'.$data['settingName'].'"';
          $result = $this->Admin_model->select_with_where('*', $condition ,'settings');
          if(count($result)>0)
          {
              $this->Admin_model->update_function('settingName', 'siteName','settings', $data);

              $message = array(
                  'type' => 'success',
                  'text' => 'Settings Update Successfully',
              );

          }else{

              $this->Admin_model->insert('settings', $data);

              $message = array(
                  'type' => 'success',
                  'text' => 'Settings Saved Successfully',
              );

          }
        }

        if(!empty($address))
        {
            $data = [];
            $data['settingName'] = 'address';
            $data['value'] = $address;
            $condition = 'settingName = "'.$data['settingName'].'"';
            $result = $this->Admin_model->select_with_where('*', $condition ,'settings');
            if(count($result)>0)
            {
                $this->Admin_model->update_function('settingName', 'address','settings', $data);


                $data['settingName'] = 'updated_at';
                $data['value'] = $this->today;

                $message = array(
                    'type' => 'success',
                    'text' => 'Settings Update Successfully',
                );

            }
        }

        if(!empty($_FILES['logo']['tmp_name']))
        {
            $data = [];
            $data['settingName'] = 'logo';
            $condition1 = 'settingName = "'.$data['settingName'].'"';
            $result_logo = $this->Admin_model->select_with_where('*', $condition1 ,'settings');


            $i_ext = explode('.', $_FILES['logo']['name']);
            $logo_path = date("d-m-Y")."_".uniqid().'.' .end($i_ext);

            $size = getimagesize($_FILES['logo']['tmp_name']);

            if (move_uploaded_file($_FILES['logo']['tmp_name'], 'uploads/logo/' . $logo_path))
            {
                $data['value'] = base_url().'uploads/logo/'.$logo_path;
            }


            if ($size[0] == 140 || $size[1] == 50) {

            }
            else {
                $imageWidth = 140; //Contains the Width of the Image

                $imageHeight = 50;

                $this->resize($imageWidth, $imageHeight, "uploads/logo/" . $logo_path, "uploads/logo/" . $logo_path);
            }


            if(count($result_logo)>0)
            {
                if($result_logo[0]['value']){
                    $image = explode(base_url(), $result_logo[0]['value']);

                    unlink( $image[1]);
                }
                $this->Admin_model->update_function('settingName', 'logo','settings', $data);

                $message = array(
                    'type' => 'success',
                    'text' => 'Settings Update Successfully',
                );
            }

        }

        if(!empty($_FILES['favicon']['tmp_name']))
        {
            $favicon = [];
            $favicon['settingName'] = 'favicon';
            $condition2 = 'settingName = "'.$favicon['settingName'].'"';
            $result_favicon = $this->Admin_model->select_with_where('*', $condition2 ,'settings');

            $i_ext = explode('.', $_FILES['favicon']['name']);
            $favicon_path = date("d-m-Y")."_".uniqid().'.' .end($i_ext);

            $size = getimagesize($_FILES['favicon']['tmp_name']);

            if (move_uploaded_file($_FILES['favicon']['tmp_name'], 'uploads/logo/' . $favicon_path))
            {
                $favicon['value'] = base_url().'uploads/logo/'.$favicon_path;
            }




            if ($size[0] == 20 || $size[1] == 20) {

            }
            else {
                $imageWidth = 20; //Contains the Width of the Image

                $imageHeight = 20;

                $this->resize($imageWidth, $imageHeight, "uploads/logo/" . $favicon_path, "uploads/logo/" . $favicon_path);
            }


            if(count($result_favicon)>0)
            {
                if($result_favicon[0]['value']){
                    $image = explode(base_url(), $result_favicon[0]['value']);

                    unlink( $image[1]);
                }
                $this->Admin_model->update_function('settingName', 'favicon','settings', $favicon);

                $message = array(
                    'type' => 'success',
                    'text' => 'Settings Update Successfully',
                );
            }


        }


        $update = [];
        $update['settingName'] = 'updated_at';
        $update['value'] = $this->today;

        $this->Admin_model->update_function('settingName', 'updated_at','settings', $update);

        $this->session->set_flashdata('message', $message);

        redirect($_SERVER['HTTP_REFERER']);
    }
}
