<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/5/2019
 * Time: 4:39 PM
 */


$apiKey = $this->session->userdata('APIKey');
$vendorID = $this->session->userdata('VendorID');
?>

<!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="float-left">
                    <h1 class="title">Edit Brand</h1>
                </div>

                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>
                            <a href=""><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="">Brands</a>
                        </li>
                        <li class="active">
                            <strong>Edit Brand</strong>
                        </li>
                    </ol>
                </div>

            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xl-12 col-lg-12 col-12 col-md-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">Update Brand</h2>
                    <div class="actions panel_actions float-right">
                        <!--                        <i class="box_toggle fa fa-chevron-down"></i>-->
                        <!--                        <i class="box_close fa fa-times"></i>-->
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <form action="" method="POST" id="editBrand">
                            <div class="col-xl-8 col-lg-8 col-md-9 col-12">

                                <div class="hidden" id="alert">
                                    <button class="close" data-close="alert"></button>
                                    <span id="message"></span>
                                </div>

                                <input type="hidden" value="<?=$apiKey?>" id="APIKey" name="APIKey">
                                <input type="hidden" value="<?=$vendorID?>" id="VendorID" name="VendorID">
                                <input type="hidden" value="<?=$result['id']?>" id="brandId" name="brandId">

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Brand Name</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" value="<?=$result['name']?>" class="form-control" id="name" name="name">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="form-label" for="field-6">Description</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <textarea id="description" class="form-control autogrow" cols="5" name="description"><?=$result['description']?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-1">Category Name</label>
                                    <span class="desc"></span>
                                    <div class="controls">

                                        <select class="form-control selectpicker" data-live-search="true" name="categoryId" id="categoryId">
                                            <?php
                                            foreach($categories as $category):
                                                ?>
                                                <option value="<?=$category['categoryId']?>">
                                                    <?=$category['name']?>
                                                </option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-6">Status</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <select class="form-control" name="status" id="status">
                                            <option value="1">Published</option>
                                            <option value="2">Unpublished</option>
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 padding-bottom-30">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 padding-bottom-30">
                                    <div class="text-right">
                                        <input type="submit" class="btn btn-primary" value="Update" />
                                        <!--                                    <button type="reset" class="btn btn-warning ">Reset</button>-->
                                        <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)">
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>


                </div>
            </section>
        </div>


    </section>
</section>
<!-- END CONTENT -->
<script type="text/javascript">
    document.forms['editBrand'].elements['status'].value=<?=$result['status']?>;
    document.forms['editBrand'].elements['categoryId'].value=<?=$result['categoryId']?>;
</script>
