<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/2/2019
 * Time: 6:24 PM
 */
?>
<!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="float-left">
                    <h1 class="title">Sub-Categories</h1>                            </div>

                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="eco-categories.html">Brands</a>
                        </li>
                        <li class="active">
                            <strong>List Brand</strong>
                        </li>
                    </ol>
                </div>

            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-lg-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">All Brands</h2>
                    <div class="actions panel_actions float-right">
                        <i class="box_toggle fa fa-chevron-down"></i>
                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                        <i class="box_close fa fa-times"></i>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">



                            <!-- ********************************************** -->



                            <table id="list" class="display table table-hover table-condensed" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Brand Name</th>
                                    <th>Brand Category</th>
                                    <th>Description</th>
                                    <th class="text-right">Manage</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php


                                foreach ($brands as $key => $brand){
                                    ?>
                                    <tr>
                                        <td><?=$key+1?></td>
                                        <td><?=$brand['name']?></td>
                                        <td><?=$brand['categoryName']?></td>
                                        <td><?=$brand['description']?></td>

                                        <td class="text-right">
                                            <a href="<?php echo base_url()."admin/edit_brand/".$brand['id']?>" class="uk-margin-left btn btn-primary"><i class="fa fa-edit"> </i> Edit</a>
                                            <a href="<?php echo base_url()."admin/delete_brand/".$brand['id']?>" onclick="return confirm('Are you Sure to Delete? ');" class="btn btn-danger"><i class="fa fa-trash"> </i> Remove</a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>


                        </div>
                    </div>
                    <div class="row text-right">
                        <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)"> 
                    </div>
                </div>
            </section>
        </div>
    </section>
</section>
<!-- END CONTENT -->

