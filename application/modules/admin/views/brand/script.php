<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/3/2019
 * Time: 6:27 PM
 */
?>



<script type="application/javascript">
    $(document).ready(function() {
        var date = new Date();
        var url = '<?php echo base_url()."admin/submit_brand";?>';
        // process the form
        $('#addBrand').submit(function(event) {

            // get the form data
            // there are many ways to get this data using jQuery (you can use the class or id also)
            var formData = {
                'APIKey': $('#APIKey').val(),
                'vendorId': $('#VendorID').val(),
                'name': $('#name').val(),
                'slug': $('#slug').val(),
                'status': $('#status').val(),
                'description': $('#description').val(),
                'categoryId': $('#categoryId').val(),
                'created': date.getTime(),
                'updated': null
            };


            console.log($('#categoryId').val());
            // process the form
            $.ajax({
                type        : 'POST',
                url         : url, // the url where we want to POST
                data        : formData,
                dataType    : 'json',
                encode      : true
            })
            // using the done promise callback
                .done(function(data) {

                    if(data==true){
                        $('#alert').removeClass();
                        $('#alert').addClass('alert alert-success');
                        $('#message').text('Successfully Brand Added');
                    }
                    if(data==false){
                        $('#alert').removeClass();
                        $('#alert').addClass('alert alert-danger');
                        $('#message').text('Unable to Add Brand');
                    }
                });

            $("#addBrand")[0].reset();

//            // stop the form from submitting the normal way and refreshing the page
            event.preventDefault();
        });


        var updateUrl = '<?php echo base_url()."admin/update_brand";?>';
        $('#editBrand').submit(function(event) {

            // get the form data
            // there are many ways to get this data using jQuery (you can use the class or id also)
            var formData = {
                'APIKey': $('#APIKey').val(),
                'vendorId': $('#VendorID').val(),
                'brandId': $('#brandId').val(),
                'name': $('#name').val(),
                'status': $('#status').val(),
                'description': $('#description').val(),
                'categoryId': $('#categoryId').val(),
                'updated': date.getTime()
            };


            // process the form
            $.ajax({
                type        : 'POST',
                url         : updateUrl, // the url where we want to POST
                data        : formData,
                dataType    : 'json',
                encode      : true
            })
            // using the done promise callback
                .done(function(data) {

                    if(data==true){
                        $('#alert').removeClass();
                        $('#alert').addClass('alert alert-success');
                        $('#message').text('Successfully Brand Updated');
                    }
                    if(data==false){
                        $('#alert').removeClass();
                        $('#alert').addClass('alert alert-danger');
                        $('#message').text('Unable to Update Brand');
                    }
                });

//            // stop the form from submitting the normal way and refreshing the page
            event.preventDefault();
        });

    });
</script>