<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/28/2018
 * Time: 10:59 AM
 */

?>
<!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="float-left">
                    <h1 class="title">JCMS Pro Rewards</h1>                            </div>


            </div>
        </div>
        <div class="clearfix"></div>


        <div class="col-lg-12 col-md-12 col-sm-12">
            <section class="box nobox">
                <div class="content-body">
                    <div class="row">
                                    <div class="col-lg-3 col-md-6 col-12">
                                        <div class="r4_counter db_box">
                                            <i class='float-left fa fa-dollar icon-md icon-rounded icon-info'></i>
                                            <div class="stats">
                                                <h4><strong><?=$orders;?></strong></h4>
                                                <span>Order Today</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-12">
                                        <div class="r4_counter db_box">
                                            <i class='float-left fa fa-shopping-cart icon-md icon-rounded icon-danger'></i>
                                            <div class="stats">
                                                <h4><strong><?=$shipments;?></strong></h4>
                                                <span>Pending Shipment</span>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="col-lg-3 col-md-6 col-12">
                                        <div class="r4_counter db_box">
                                            <i class='float-left fa fa-dollar icon-md icon-rounded icon-info'></i>
                                            <div class="stats">
                                                <h4><strong><?=$orders;?></strong></h4>
                                                <span>Total Order</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-12">
                                        <div class="r4_counter db_box">
                                            <i class='float-left fa fa-shopping-cart icon-md icon-rounded icon-success'></i>
                                            <div class="stats">
                                                <h4><strong><?=$shipments;?></strong></h4>
                                                <span>Completed Shipment</span>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-lg-3 col-md-6 col-12">
                                        <div class="r4_counter db_box">
                                            <i class='float-left fa fa-cubes icon-md icon-rounded icon-info'></i>
                                            <div class="stats">
                                                <h4><strong><?=$products;?></strong></h4>
                                                <span>New Products</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-12">
                                        <div class="r4_counter db_box">
                                            <i class='float-left fa fa-cubes icon-md icon-rounded icon-info'></i>
                                            <div class="stats">
                                                <h4><strong><?=$products;?></strong></h4>
                                                <span>Total Products</span>
                                            </div>
                                        </div>
                                    </div> 
                                     <div class="col-lg-3 col-md-6 col-12">
                                        <div class="r4_counter db_box">
                                            <i class='float-left fa fa-user icon-md icon-rounded icon-info'></i>
                                            <div class="stats">
                                                <h4><strong><?=$users;?></strong></h4>
                                                <span>New Users</span>
                                            </div>
                                        </div>
                                    </div>
                                  <div class="col-lg-3 col-md-6 col-12">
                                        <div class="r4_counter db_box">
                                            <i class='float-left fa fa-users icon-md icon-rounded icon-info'></i>
                                            <div class="stats">
                                                <h4><strong><?=$users;?></strong></h4>
                                                <span>Total Users</span>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- End .row -->    

                </div>
            </section>
        </div>
        <div class="col-lg-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">Latest Orders</h2>
                    <div class="actions panel_actions float-right">
                    </div>
                </header>
                <div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">



                            <!-- ********************************************** -->


                            <table class=" table" cellspacing="0" width="100% ">
                                <tr width="100% ">
                                    <th>Order Ref</th>
                                    <th>Customer ID</th>
                                    <th>Product ID</th>
                                    <th>Product SKU</th>
                                    <th>Payment Status</th>
                                    <th>Status</th>
                                    <th>Order Remark</th>
                                    <!--                                    <th>Manage</th>-->
                                </tr>

                                <?php
                                for($i=0; $i<5; $i++){
                                    ?>
                                    <tr width="100% ">
                                        <td>545<?=$i?></td>
                                        <td>0989988<?=$i?></td>
                                        <td>JP0<?=$i?>1</td>
                                        <td><?=$i?>2</td>
                                        <td>1234<?=$i?>5566</td>
                                        <td><?php if($i%2==0) { echo "1"; }else{ echo "0"; } ?></td>
                                        <td>Points Only Payment</td>
                                        <!--                                    <td>-->
                                        <!--                                        <a href="">Edit</a> || <a href="">Delete</a>-->
                                        <!--                                    </td>-->
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                            <!-- ********************************************** -->




                        </div>
                    </div>

                </div>
            </section></div>
             <!--table-->

 <!--/table-->
        <div class="row margin-0">
<!--             <div class="col-lg-4 col-md-12 col-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title float-left">New Category</h2>
                                <div class="actions panel_actions float-right">
                                    <i class="box_toggle fa fa-chevron-down"></i>
                                    <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                                    <i class="box_close fa fa-times"></i>
                                </div>
                            </header>
                            <div class="content-body">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width:60%">Name</th>
                                        <th style="width:30%">Progress</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>

                                        <td>Harry P.</td>
                                        <td><span class="playlist_song2">...</span></td>
                                    </tr>
                                    <tr>

                                        <td>Will Mark</td>
                                        <td><span class="playlist_song3">...</span></td>
                                    </tr>
                                    <tr>

                                        <td>Jason D.</td>
                                        <td><span class="playlist_song4">...</span></td>
                                    </tr>

                                    <tr>

                                        <td>Nik P.</td>
                                        <td><span class="playlist_song6">...</span></td>
                                    </tr>
                                    <tr>

                                        <td>Kate Wilson</td>
                                        <td><span class="playlist_song7">...</span></td>
                                    </tr>
                                    </tbody>
                                </table>


                            </div>
                        </section>

                    </div>
                </div>
            </div>
 -->
<!--             <div class="col-lg-4 col-md-12 col-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title float-left">New Vendors</h2>
                                <div class="actions panel_actions float-right">
                                    <i class="box_toggle fa fa-chevron-down"></i>
                                    <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                                    <i class="box_close fa fa-times"></i>
                                </div>
                            </header>
                            <div class="content-body">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width:60%">Name</th>
                                        <th style="width:30%">Progress</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>

                                        <td>John Doug</td>
                                        <td><span class="playlist_song1">...</span></td>
                                    </tr>
                                    <tr>

                                        <td>Harry P.</td>
                                        <td><span class="playlist_song2">...</span></td>
                                    </tr>

                                    <tr>

                                        <td>Clarke M.</td>
                                        <td><span class="playlist_song5">...</span></td>
                                    </tr>
                                    <tr>

                                        <td>Nik P.</td>
                                        <td><span class="playlist_song6">...</span></td>
                                    </tr>
                                    <tr>

                                        <td>Kate Wilson</td>
                                        <td><span class="playlist_song7">...</span></td>
                                    </tr>
                                    </tbody>
                                </table>


                            </div>
                        </section>

                    </div>
                </div>
            </div> -->

<!--             <div class="col-lg-4 col-md-12 col-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title float-left">New Customers</h2>
                                <div class="actions panel_actions float-right">
                                    <i class="box_toggle fa fa-chevron-down"></i>
                                    <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                                    <i class="box_close fa fa-times"></i>
                                </div>
                            </header>
                            <div class="content-body">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width:60%">Name</th>
                                        <th style="width:30%">Progress</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>

                                        <td>Harry P.</td>
                                        <td><span class="playlist_song2">...</span></td>
                                    </tr>
                                    <tr>

                                        <td>Will Mark</td>
                                        <td><span class="playlist_song3">...</span></td>
                                    </tr>
                                    <tr>

                                        <td>Jason D.</td>
                                        <td><span class="playlist_song4">...</span></td>
                                    </tr>
                                    <tr>

                                        <td>Clarke M.</td>
                                        <td><span class="playlist_song5">...</span></td>
                                    </tr>
                                    <tr>

                                        <td>Nik P.</td>
                                        <td><span class="playlist_song6">...</span></td>
                                    </tr>

                                    </tbody>
                                </table>


                            </div>
                        </section>

                    </div>
                </div>
            </div> -->
        </div>

    </section>
</section>
<!-- END CONTENT -->
