<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/28/2018
 * Time: 11:15 AM
 */
?>
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
<script src="<?php echo base_url();?>/admin-assets/assets/plugins/jquery-ui/smoothness/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>/admin-assets/assets/plugins/sparkline-chart/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>/admin-assets/assets/plugins/easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>/admin-assets/assets/plugins/morris-chart/js/raphael-min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>/admin-assets/assets/plugins/morris-chart/js/morris.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>/admin-assets/assets/js/eco-dashboard.js" type="text/javascript"></script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->

