<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/6/2019
 * Time: 5:51 PM
 */

?>


<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="float-left">
                    <h1 class="title">Order History</h1>                            </div>

                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>
                            <a href="#"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="#">Orders</a>
                        </li>
                        <li class="active">
                            <strong>All Orders</strong>
                        </li>
                    </ol>
                </div>

            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-lg-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">Order Details</h2>
                    <div class="actions panel_actions float-right">
<!--                        <i class="box_toggle fa fa-chevron-down"></i>-->
<!--                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>-->
<!--                        <i class="box_close fa fa-times"></i>-->
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">



                            <!-- ********************************************** -->


                            <table id="list" class=" table table-hover table-condensed" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Order Ref</th>
                                    <th>Customer ID</th>
<!--                                    <th>Product Order Ref</th>-->
                                    <th>Product ID</th>
<!--                                    <th>Product Name</th>-->
                                    <th>Product SKU</th>
                                    <th>Payment Status</th>
                                    <th>Status</th>
<!--                                    <th>Order Date</th>-->
                                    <th>Order Remark</th>
                                    <th>Manage</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php
                                for($i=0; $i<50; $i++){
                                ?>
                                <tr>
                                    <td>545<?=$i?></td>
                                    <td>0989988<?=$i?></td>
<!--                                    <td>CC00--><?//=$i?><!--09--><?//=$i?><!--9--><?//=$i?><!--99</td>-->
                                    <td>JP0<?=$i?>1</td>
<!--                                    <td>Null</td>-->
                                    <td>1234<?=$i?>5566</td>
                                    <td><?php if($i%2==0) { echo "Paid"; }else{ echo "Unpaid";  } ?></td>
                                    <td><?php if($i%2==0) { echo "Done"; }else{ echo "Waiting";  } ?></td>
<!--                                    <td>Points Only Payment</td>-->
                                    <td>Points Only Payment</td>
                                    <td>
                                        <a href="#" class="btn btn-primary"> <i class="fa fa-eye"> </i> View </a>
                                    </td>
                                </tr>
                                <?php
                                }
                                ?>

                                </tbody>
                            </table>
                            <!-- ********************************************** -->




                        </div>
                    </div>
                    <div class="row text-right">
                        <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)">  
                    </div>
                </div>
            </section></div>
    </section>
</section>
<!-- END CONTENT -->
