<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/8/2019
 * Time: 4:11 PM
 */
?>


<script type="application/javascript">

    $(document).ready(function(){
        var table = $('#purchase_list').DataTable({
            columnDefs: [
                { width: 200, targets: 0 }
            ],
            fixedColumns: true
        } );
    });
</script>
