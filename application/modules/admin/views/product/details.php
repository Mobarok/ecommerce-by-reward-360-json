<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/6/2019
 * Time: 5:22 PM
 */
?>

<!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="float-left">
                    <h1 class="title">Product Details</h1>                            </div>

                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="ui-pricing.html">Pages</a>
                        </li>
                        <li class="active">
                            <strong>Profile</strong>
                        </li>
                    </ol>
                </div>

            </div>
        </div>
        <div class="clearfix"></div>


        <div class="col-xl-12">
            <section class="box nobox">
                <div class="content-body">
                    <div class="row">
                        <div class="col-lg-5 col-md-6 col-12">

                            <table id="example-11" class="display table table-hover table-condensed" cellspacing="0" width="100%">
                                <thead>

                                </thead>

                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td colspan=3">1</td>
                                </tr>
                                <tr>
                                    <th>Description</th>
                                    <td colspan=3">Product Description</td>
                                </tr>
                                <tr>
                                    <th>Short Description</th>
                                    <td colspan=3">This is a short description</td>
                                </tr>
                                <tr>
                                    <th>Product Category</th>
                                    <td colspan=3">Category 1</td>
                                </tr>
                                <tr>
                                    <th>Brand</th>
                                    <td colspan=3">Brand Name</td>
                                </tr>
                                <tr>
                                    <th>Display Order</th>
                                    <td colspan=3">1</td>
                                </tr>
                                <tr>
                                    <th>Product Type</th>
                                    <td colspan=3">WorldMils</td>
                                </tr>
                                <tr>
                                    <th>Redemption Type</th>
                                    <td colspan=3">Points Plus Cash</td>
                                </tr>
                                <tr>
                                    <th>Success Template ID</th>
                                    <td colspan=3">1009</td>
                                </tr>
                                <tr>
                                    <th>Failure Template ID</th>
                                    <td colspan=3">9009</td>
                                </tr>
                                <tr>
                                    <th>Voucher ID</th>
                                    <td colspan=3">8809TR</td>
                                </tr>
                                <tr>
                                    <th>Product Price</th>
                                    <td colspan=3">$890</td>
                                </tr>
                                <tr>
                                    <th>Product Price: MRP</th>
                                    <td colspan=3">$890</td>
                                </tr>
                                <tr>
                                    <th>Product Price: Client</th>
                                    <td colspan=3">$890</td>
                                </tr>
                                <tr>
                                    <th>Product Price: Country Code</th>
                                    <td colspan=3">C890</td>
                                </tr>
                                <tr>
                                    <th>Product Price: Minimum Points</th>
                                    <td colspan=3">$890</td>
                                </tr>
                                <tr>
                                    <th>Product Price: Actual Points</th>
                                    <td colspan=3">$890</td>
                                </tr>
                                <tr>
                                    <th>Product Price: Ratio</th>
                                    <td colspan=3">$890</td>
                                </tr>
                                <tr>
                                    <th>Stock Count</th>
                                    <td colspan=3">90</td>
                                </tr>
                                <tr>
                                    <th>Active</th>
                                    <td colspan=3">On</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td colspan=3">Pending</td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                        <div class="col-lg-7 col-md-6 col-12">

                            <!-- Product image -->
                            <div class="col-xl-12">
                                <section class="box ">

                                    <div class="content-body">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-12">

                                                <!-- start -->
                                                <div id='portfolio' class="">
                                                    <div class="text-center">
                                                        <ul class="portfolio-filter list-inline">
                                                            <li><a class="btn btn-primary active" href="#" data-filter="*">All  Images By Product</a></li>

                                                        </ul><!--/#portfolio-filter-->
                                                    </div>

                                                    <div class="portfolio-items">
                                                        <div class="portfolio-item col-lg-6 col-md-6 col-12  creative">
                                                            <div class="portfolio-item-inner">
                                                                <img class="img-fluid" src="<?=base_url()?>admin-assets/data/gallery/01.jpg" alt="">
                                                                <div class="portfolio-info animated fadeInUp animated-duration-600ms">
                                                                    <h3>Portfolio Item 1</h3>
                                                                    <span class='desc'>A brief description area for the portfolio item.</span>
                                                                    <a class="preview" href="data/gallery/01.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                                                                </div>
                                                            </div>
                                                        </div><!--/.portfolio-item-->

                                                        <div class="portfolio-item col-lg-6 col-md-6 col-12  corporate portfolio">
                                                            <div class="portfolio-item-inner">
                                                                <img class="img-fluid" src="<?=base_url()?>admin-assets/data/gallery/02.jpg" alt="">
                                                                <div class="portfolio-info animated fadeInUp animated-duration-600ms">
                                                                    <h3>Portfolio Item 2</h3>
                                                                    <span class='desc'>A brief description area for the portfolio item.</span>
                                                                    <a class="preview" href="data/gallery/02.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                                                                </div>
                                                            </div>
                                                        </div><!--/.portfolio-item-->

                                                        <div class="portfolio-item col-lg-6 col-md-6 col-12  creative">
                                                            <div class="portfolio-item-inner">
                                                                <img class="img-fluid" src="<?=base_url()?>admin-assets/data/gallery/03.jpg" alt="">
                                                                <div class="portfolio-info animated fadeInUp animated-duration-600ms">
                                                                    <h3>Portfolio Item 3</h3>
                                                                    <span class='desc'>A brief description area for the portfolio item.</span>
                                                                    <a class="preview" href="data/gallery/03.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                                                                </div>
                                                            </div>
                                                        </div><!--/.portfolio-item-->

                                                        <div class="portfolio-item col-lg-6 col-md-6 col-12  corporate">
                                                            <div class="portfolio-item-inner">
                                                                <img class="img-fluid" src="<?=base_url()?>admin-assets/data/gallery/04.jpg" alt="">
                                                                <div class="portfolio-info animated fadeInUp animated-duration-600ms">
                                                                    <h3>Portfolio Item 4</h3>
                                                                    <span class='desc'>A brief description area for the portfolio item.</span>
                                                                    <a class="preview" href="data/gallery/04.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                                                                </div>
                                                            </div>
                                                        </div><!--/.portfolio-item-->

                                                        <div class="portfolio-item col-lg-6 col-md-6 col-12  creative portfolio">
                                                            <div class="portfolio-item-inner">
                                                                <img class="img-fluid" src="<?=base_url()?>admin-assets/data/gallery/05.jpg" alt="">
                                                                <div class="portfolio-info animated fadeInUp animated-duration-600ms">
                                                                    <h3>Portfolio Item 5</h3>
                                                                    <span class='desc'>A brief description area for the portfolio item.</span>
                                                                    <a class="preview" href="data/gallery/05.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                                                                </div>
                                                            </div>
                                                        </div><!--/.portfolio-item-->

                                                        <div class="portfolio-item col-lg-6 col-md-6 col-12  corporate">
                                                            <div class="portfolio-item-inner">
                                                                <img class="img-fluid" src="<?=base_url()?>admin-assets/data/gallery/06.jpg" alt="">
                                                                <div class="portfolio-info animated fadeInUp animated-duration-600ms">
                                                                    <h3>Portfolio Item 5</h3>
                                                                    <span class='desc'>A brief description area for the portfolio item.</span>
                                                                    <a class="preview" href="data/gallery/06.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                                                                </div>
                                                            </div>
                                                        </div><!--/.portfolio-item-->

                                                        <div class="portfolio-item col-lg-6 col-md-6 col-12  creative portfolio">
                                                            <div class="portfolio-item-inner">
                                                                <img class="img-fluid" src="<?=base_url()?>admin-assets/data/gallery/07.jpg" alt="">
                                                                <div class="portfolio-info animated fadeInUp animated-duration-600ms">
                                                                    <h3>Portfolio Item 7</h3>
                                                                    <span class='desc'>A brief description area for the portfolio item.</span>
                                                                    <a class="preview" href="data/gallery/07.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                                                                </div>
                                                            </div>
                                                        </div><!--/.portfolio-item-->

                                                        <div class="portfolio-item col-lg-6 col-md-6 col-12  corporate">
                                                            <div class="portfolio-item-inner">
                                                                <img class="img-fluid" src="<?=base_url()?>admin-assets/data/gallery/08.jpg" alt="">
                                                                <div class="portfolio-info animated fadeInUp animated-duration-600ms">
                                                                    <h3>Portfolio Item 8</h3>
                                                                    <span class='desc'>A brief description area for the portfolio item.</span>
                                                                    <a class="preview" href="data/gallery/08.jpg" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                                                                </div>
                                                            </div>
                                                        </div><!--/.portfolio-item-->
                                                    </div>
                                                </div>
                                                <!-- end -->








                                            </div>
                                        </div>
                                        <div class="row text-right">
                                            <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)">  
                                        </div>
                                    </div>
                                </section>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div>


    </section>
</section>
<!-- END CONTENT -->


