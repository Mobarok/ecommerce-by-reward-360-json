<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/5/2019
 * Time: 5:57 PM
 */


$apiKey = $this->session->userdata('APIKey');
$vendorID = $this->session->userdata('VendorID');
?>

<style type="text/css">
    .multiselect-native-select .btn-group {
        display: none;
    }
</style>

<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="float-left">
                    <h1 class="title">Edit Products</h1>                            </div>

                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>
                            <a href=""><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="">Products</a>
                        </li>
                        <li class="active">
                            <strong>Edit Products</strong>
                        </li>
                    </ol>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-xl-12 col-lg-12 col-12 col-md-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">Update product info</h2>
                    <div class="actions panel_actions float-right">
                        <i class="box_toggle fa fa-chevron-down"></i>
                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                        <i class="box_close fa fa-times"></i>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">

                        <form action="" method="post" id="editProduct" enctype="multipart/form-data">
                            <div class="col-xl-8 col-lg-8 col-md-9 col-12">


                                <div class="hidden" id="alert">
                                    <button class="close" data-close="alert"></button>
                                    <span id="message"></span>
                                </div>

                                <input type="hidden" value="<?=$apiKey?>" id="APIKey" name="APIKey">
                                <input type="hidden" value="<?=$vendorID?>" id="VendorID" name="VendorID">

                                <div class="form-group">
                                    <label class="form-label" for="name">Name</label>
                                    <span class="desc text-danger">*</span>
                                    <div class="controls">
                                        <input type="text" value="" required class="form-control" id="name">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="name">Product ID</label>
                                    <span class="desc text-danger">*</span>
                                    <div class="controls">
                                        <input type="text"  readonly value="" class="form-control" id="productID" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="name">Product SKU</label>
                                    <span class="desc text-danger">*</span>
                                    <div class="controls">
                                        <input type="text" value="" required class="form-control" id="sku">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="description">Specifications</label>
                                    <span class="desc "></span>
                                    <div class="controls">
                                        <textarea class="form-control autogrow" cols="5" id="specifications"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="description">Description</label>
                                    <span class="desc text-danger">*</span>
                                    <div class="controls">
                                        <textarea class="form-control autogrow" required cols="5" id="description"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="short_description">Short Description</label>
                                    <span class="desc text-danger">*</span>
                                    <div class="controls">
                                        <textarea required class="form-control autogrow" cols="5" id="shortDescription"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="product_categories">Product Categories</label>
                                    <span class="desc text-danger">*</span>
                                    <select required class="form-control selectpicker" multiple  id="productCategories" data-live-search="true">
                                        <?php
                                        foreach ($categories as $category)
                                        {
                                            echo "<option value='".$category['categoryId']."'>".$category['name']."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="brand">Brand</label>
                                    <span class="desc"></span>
                                    <select class="form-control selectpicker" id="brand" data-live-search="true">
                                        <option value="">Nothing Selected</option>
                                        <?php
                                        foreach ($brands as $brand)
                                        {
                                            echo "<option value='".$brand['name']."'>".$brand['name']."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="image">Product Image</label>
                                    <span class="desc text-danger">*</span>
                                    <div class="controls">
                                        <input type="file" class="form-control" id="image" multiple>
                                    </div>


                                </div>
                                <div id="setImage" style=" width: 100%; overflow: hidden;">

                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="display_order">Display Order</label>
                                    <span class="desc "></span>
                                    <div class="controls">
                                        <input type="number" class="form-control" id="displayOrder">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="product_type">Product Type</label>
                                    <span class="desc text-danger">*</span>
                                    <select required class="form-control selectpicker" name="productType" id="productType" data-live-search="true" >
                                        <option value="1">Points</option>
                                        <option value="2">Cashback</option>
                                        <option value="3">WorldMiles</option>
                                        <option value="4">Ultimate</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="redemption">Redemption Type</label>
                                    <span class="desc text-danger">*</span>
                                    <select required class="form-control selectpicker" name="redemptionType" id="redemptionType" data-live-search="true">
                                        <option value="1">Points</option>
                                        <option value="2">Points plus cash</option>
                                        <option value="3">Cash Only</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="success_temp_id">Success Template ID</label>
                                    <span class="desc text-danger"> * </span>
                                    <div class="controls">
                                        <input type="text" value=" " class="form-control" required id="successTempleteId">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="failure_temp_id">Failure Template ID</label>
                                    <span class="desc text-danger"> * </span>
                                    <div class="controls">
                                        <input type="text" value=" " class="form-control" required id="failureTempleteId">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="voucher_id">Voucher ID</label>
                                    <span class="desc text-danger"> * </span>
                                    <div class="controls">
                                        <input type="text" required class="form-control" id="voucherId">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="product_price">Currency Code</label>
                                    <span class="desc text-danger"> * </span>
                                    <select class="form-control" required id="currencyCode" style="height: 34px !important;">
                                        <option value="RM"> RM </option>
                                        <option value="USD"> USD </option>
                                        <option value="MYR"> MYR </option>
                                        <option value="INR"> INR </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="product_price_mrp">Product Price: MRP</label>
                                    <span class="desc text-danger">*</span>
                                    <div class="controls">
                                        <input type="number" value="0" min="0" required class="form-control" id="mrp">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="product_price_client">Product Price: Client Price</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="number" value="0" min="0" class="form-control" id="clientPrice">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="product_price_points">Product Price: Minimum Points</label>
                                    <span class="desc text-danger">*</span>
                                    <div class="controls">
                                        <input type="number" value="0" min="0" required class="form-control" id="minimumPoints">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="product_price_cash">Product Price: Minimum Cash</label>
                                    <span class="desc text-danger"> * </span>
                                    <div class="controls">
                                        <input type="number" value="0" required min="0" class="form-control" id="minimumCash">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" value="0" for="product_price_ap">Product Price: Discount</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="number"  value="0" min="0" class="form-control" id="discount">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="product_price_ap">Product Price: Base Points</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="number" value="0" min="0" class="form-control" id="basePoints">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="product_price_ap">Product Price: Actual Points </label>
                                    <span class="desc text-danger"> * </span>
                                    <div class="controls">
                                        <input type="number" value="0" min="0" required class="form-control" id="actualPoints">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="product_price_ratio">Product Price: Ratio</label>
                                    <span class="desc text-danger">*</span>
                                    <div class="controls">
                                        <input type="number" value="0" min="0" required class="form-control" id="ratio">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="stock">Quantity</label>
                                    <span class="desc text-danger"> * </span>
                                    <div class="controls">
                                        <input type="number" value="0" min="0" class="form-control" id="stockCount">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="active">Active</label>
                                    <span class="desc text-danger">*</span>
                                    <select class="form-control" name="active" required id="active">
                                        <option value="true">On</option>
                                        <option value="false">Off</option>
                                    </select>
                                </div>

                                <!--  Note: Status Value can be change After API -->
                                <div class="form-group">
                                    <label class="form-label" for="status">Status</label>
                                    <span class="desc text-danger">*</span>
                                    <select class="form-control" name="status" required id="status">
                                        <option value="0"> Active</option>
                                        <option value="1"> Pending</option>
                                        <option value="2"> Delete</option>
                                    </select>
                                </div>


                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 padding-bottom-30">
                                <div class="text-right">
                                    <input type="submit" class="btn btn-primary" value="Update" />
                                    <!--                                    <button type="reset" class="btn btn-warning ">Reset</button>-->
                                    <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)">
                                </div>
                            </div>

                    </div>
</form>
</div>

</section>
</div>

</section>

</section>