<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/8/2019
 * Time: 6:54 PM
 */
?>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>


<script type="application/javascript">


    $(document).ready(function() {
        var date = new Date();

        var images = [];

        var obj = {
            "id":null,
            "imageId":null,
            "url":null,
            "size":1,
            "displayOrder":1,
            "type":0,
            "productId":null,
            "vendor":null,
            "alt":null
        };

        images.push(obj);


    var editProductId = "<?php echo $this->session->userdata('editProductId') ?>";



    var $this = $('#editProduct');

    var getProductById = "<?php echo base_url().'/admin/getProductById/'?>"+editProductId;
    $.ajax({
        url: getProductById,
        type: 'get',
        success: function(data){
            //If the success function is execute,
            //then the Ajax request was successful.
            //Add the data we received in our Ajax
            //request to the "content" div.

//            console.log(data);
            var dataSet  = JSON.parse(data.values);
            console.log(dataSet);
            $("#name").val(dataSet.name);
            $("#sku").val(dataSet.sku);
            $("#description").val(dataSet.description);
            $("#shortDescription").val(dataSet.shortDescription);
            $("#successTempleteId").val(dataSet.successTempleteId);
            $("#failureTempleteId").val(dataSet.failureTempleteId);
            $("#voucherId").val(dataSet.voucherId);
            $("#productID").val(dataSet.name);
            $("#specifications").val(dataSet.specifications);
            $("#displayOrder").val(dataSet.displayOrder);
            $("#stockCount").val(dataSet.stockCount);

            //this will change After API

            $("#mrp").val(dataSet.productPrice.mrp);
            $("#clientPrice").val(dataSet.productPrice.clientPrice);
            $("#minimumPoints").val(dataSet.productPrice.minimumPoints);
            $("#minimumCash").val(dataSet.productPrice.minimumCash);
            $("#discount").val(dataSet.productPrice.discount);
            $("#basePoints").val(dataSet.productPrice.basePoints);
            $("#actualPoints").val(dataSet.productPrice.actualPoints);
            $("#ratio").val(dataSet.productPrice.ratio);

            //End this will change After API


            var productCategories = JSON.parse(dataSet.productCategories);

//            var array = JSON.parse(productCategories);
//            productCategories = productCategories.split(",");
//            console.log('productCategories: ');
//            console.log(productCategories);
//            console.log(array);
            var i;
            var count = productCategories.length;
            for(i=0; i<count; i++)
            {
                $('#productCategories').multiselect('select', productCategories[i]);
            }


            var productImages = dataSet.productImages;

            if(productImages.length>0)
                images = productImages;

            console.log(images);
            for(i=0; i<productImages.length; i++) {

                var deleteImage = "<?=base_url() . 'delete_image/product/'?>";
//                deleteImage = deleteImage + dataSet[i].productImageId;

                if (productImages[i].url != null) {
                    var img = "<img width='100px' style='margin: 10px;' src='" + productImages[i].url + "' /> <a href=\"#\" class=\"removeImg\" data-item='" + productImages[i].url + "'  data-value='"+i+"' data-source='"+productImages[i].id+"'  style=\"top: 0; background-color: #0a0c0a; color: #fff; border-radius:50px; font-size:0.9em; padding: 0 0.3em 0; text-align:center; margin-left: -20px; vertical-align: top; text-decoration: none; cursor:pointer;\" > X </a>";
                }else {
                    var img = "<b> Image Not Found</b>";
                }
                    $("#setImage").append(
                        "<div style='width: 125px; float: left; margin: 5px; overflow: hidden;'>" +
                         img+
                        //                        "<a class='btn btn-danger' onclick='return confirm();' href="+deleteImage+"><i class='fa fa-trash'><i></a>" +
                        "</div>"
                    )

            }


            document.forms['editProduct'].elements['productType'].value=dataSet.productType;
            document.forms['editProduct'].elements['redemptionType'].value=dataSet.redemptionType;
            document.forms['editProduct'].elements['currencyCode'].value=dataSet.currencyCode;
            document.forms['editProduct'].elements['brand'].value=dataSet.brand;
            document.forms['editProduct'].elements['status'].value=parseInt(dataSet.status);

            $('.selectpicker').selectpicker('refresh');

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorMsg = 'Ajax request failed: ' + xhr.responseText;
            console.log(errorMsg);
        }
    });


        var url = '<?php echo base_url()."admin/update_product";?>';
        // process the form


        $('#editProduct').submit(function(event) {

            // get the form data
            // there are many ways to get this data using jQuery (you can use the class or id also)
            var formData = {
                'APIKey': $('#APIKey').val(),
                'vendorId': $('#VendorID').val(),
                'productId': $('#productID').val(),
                'countryId': 1,
                'name': $('#name').val(),
                'specifications': $('#specifications').val(),
                'description': $('#description').val(),
                'productCategories': $('#productCategories').val(),
                'similarProducts': null,
                'active': $('#active').val(),
                'brand': $('#brand').val(),
                'stockCount': $('#stockCount').val(),
                'sku': $('#sku').val(),
                'validFrom': null,
                'validTill': null,
                'customerCategory': [0],
                'customerFlags': [0],
                'displayOrder': $('#displayOrder').val(),
                'vendor': $('#VendorID').val(),
                'productImages': JSON.stringify(images),
                'productPrice':{
                    "mrp": $('#mrp').val(),
                    "clientPrice": $('#clientPrice').val(),
                    "currencyCode": $('#currencyCode').val(),
                    "discount": $('#discount').val(),
                    "minimumPoints": $('#minimumPoints').val(),
                    "minimumCash": $('#minimumCash').val(),
                    "basePoints": $('#basePoints').val(),
                    "actualPoints": $('#actualPoints').val(),
                    "ratio": $('#ratio').val(),
                    "productId": $('#productID').val(),
                    "vendor": $('#VendorID').val(),
                },
                'created': date.getTime(),
                'status': $('#status').val(),
                'vendorProductStatus': null,
                'shortDescription': $('#shortDescription').val(),
                'similarProductsByVendor': null,
                'vendorSku': null,
                'partnerId': $('#VendorID').val(),
                'productType': $('#productType').val(),
                'successTempleteId': $('#successTempleteId').val(),
                'failureTempleteId': $('#failureTempleteId').val(),
                'voucherId': $('#voucherId').val(),
                'keyWords': '',
                'keyWords': '',
                'subCustomerCategory': 0,
                'redemptionType': $('#redemptionType').val(),
                'vendorCategory': "",
                'id': $('#VendorID').val(),

                'updateProductId': editProductId // Please remove After API
            };


            console.log(formData);
            // process the form
            $.ajax({
                type        : 'POST',
                url         : url, // the url where we want to POST
                data        : formData,
                dataType    : 'json',
                encode      : true
            })
            // using the done promise callback
                .done(function(data) {

                    console.log(data);

                    if(data.status==0){
//                        $('#alert').removeClass();
//                        $('#alert').addClass('alert alert-success');
//                        $('#message').text(data.statusDesc);

                        localStorage.setItem('messageType', 'success');
                        localStorage.setItem('messageText', 'You have successfully updated a Product');


                        var url = "<?=base_url().'admin/products'?>";
                        window.location.href = url;
                    }
                    if(data.status==1){
                        $('#alert').removeClass();
                        $('#alert').addClass('alert alert-danger');
                        $('#message').text(data.statusDesc);
                    }
                });

//            $("#editProduct")[0].reset();

//            setTimeout(location.reload(), 18000);
//            // stop the form from submitting the normal way and refreshing the page
//            return false;
            event.preventDefault();
        });


        var imageUpload = "<?php echo base_url()?>/admin/uploadImage/product";
    $("#image").change(function() {

        var files = $('#image')[0].files;
        var error = '';
        var form_data = new FormData();
        form_data.append("alt", $("#name").val());
        form_data.append("productId", $("#productID").val());
        form_data.append("vendor", $("#VendorID").val());
        for(var count = 0; count<files.length; count++)
        {
            var name = files[count].name;
            var extension = name.split('.').pop().toLowerCase();
            if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)
            {
                error += "Invalid " + count + " Image File"
            }
            else
            {
                form_data.append("files[]", files[count]);
            }
        }
        if(error == '')
        {
            $.ajax({
                url: imageUpload,
                method:"POST",
                data:form_data,
                contentType:false,
                cache:false,
                processData:false,
                beforeSend:function()
                {
//                        $('#uploaded_images').html("<label class='text-success'>Uploading...</label>");
                },
                success:function(data)
                {
                    if(images[0].url==null)
                    {
                        images = data.uploadLink;
                    }else{
                        images = images.filter(function (el) {
                            return el != null;
                        });

                        var setImages = data.uploadLink;
                        images = images.concat(setImages);
                    }

                    console.log(images);

                    $("#setImage").text('');

                    for(i=0; i<images.length; i++)
                    {
//                            var deleteImage = "<?//=base_url().'delete_image/product/'?>//";
//                            deleteImage = deleteImage+dataSet[i].productImageId;
                        $("#setImage").append(
                            "<div style='width: 125px; float: left; margin: 5px; overflow: hidden;'>" +
                            "<img width='100px'   style='margin: 10px;' src='"+images[i].url+"' /><a href=\"#\" class=\"removeImg\" data-item='"+images[i].url+"' data-value='"+i+"' data-source='"+images[i].id+"' style=\"top: 0; background-color: #0a0c0a; color: #fff; border-radius:50px; font-size:0.9em; padding: 0 0.3em 0; text-align:center; margin-left: -20px; vertical-align: top; text-decoration: none; cursor:pointer;\" > X </a> " +
                            "</div>"
                        )
                    }
                }
            })
        }
        else
        {
            alert(error);
        }
    });
        var messageText = localStorage.getItem("messageText");
        var messageType = localStorage.getItem("messageType");
        if( messageText !='' && messageType !='')
        {
            $('#alert').removeClass('hidden').addClass('alert');
            $('#alert').addClass('alert-success');
            $('#message').text(messageText);

            localStorage.setItem('messageText', '');
            localStorage.setItem('messageType', '');

        }



        $('#setImage').on('click', '.removeImg', function() {

            if(confirm("Are you Sure to Delete Image? ") == false)
            {
                return false;
            }else{

                var i = $( this ).data('value');
                images.splice(-i,1);
                $(this).parent().hide();
                console.log(images);

                if(images.length==0)
                {
                    images.push(obj);
                }


                console.log(images);

                var data = {
                    'url': $(this).data('item'),
                    'id': $(this).data('source')
                };

                var removeUpload = "<?=base_url().'admin/removeImage'?>";
                $.ajax({
                    url: removeUpload,
                    method:"POST",
                    data: data,
                    success: function(resultData) { console.log("Save Complete") }
                });
            }



        });

});


</script>
