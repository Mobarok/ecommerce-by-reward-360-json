<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/5/2019
 * Time: 5:57 PM
 */

?>


<!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="float-left">
                    <h1 class="title">Products List</h1>
                </div>

                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>
                            <a href="#"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="#">Products</a>
                        </li>
                        <li class="active">
                            <strong>All Products</strong>
                        </li>
                    </ol>
                </div>

            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-lg-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">All Products</h2>
                    <div class="actions panel_actions float-right">
<!--                        <i class="box_toggle fa fa-chevron-down"></i>-->
<!--                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>-->
<!--                        <i class="box_close fa fa-times"></i>-->
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="hidden" id="alert">
                                <button class="close" data-close="alert"></button>
                                <span id="message"></span>
                            </div>

                            <table id="productList" class="display table table-hover table-condensed" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Short Description</th>
<!--                                        <th>Product Category</th>-->
                                        <th>Stock</th>
                                        <th>Brand</th>
                                        <th class="text-center">Manage</th>
                                    </tr>
                                </thead>

                                <tbody>

                                </tbody>
                            </table>
                            <!-- ********************************************** -->




                        </div>
                    </div>
                    <div class="row text-right">
                        <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)">  
                    </div>
                </div>
            </section></div>
    </section>
</section>
<!-- END CONTENT -->

