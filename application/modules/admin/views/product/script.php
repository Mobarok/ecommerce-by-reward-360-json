<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/5/2019
 * Time: 6:19 PM
 */
?>

<script type="application/javascript">
    $(document).ready(function() {
        var date = new Date();
        var url = '<?php echo base_url()."admin/create_product";?>';
        // process the form

        var images = [];

        var obj = {
            "id":null,
            "imageId":null,
            "url":null,
            "size":1,
            "displayOrder":1,
            "type":0,
            "productId":null,
            "vendor":null,
            "alt":null
        };

        images.push(obj);

        $('#addProduct').submit(function(event) {

            // get the form data
            // there are many ways to get this data using jQuery (you can use the class or id also)
            var formData = {
                'APIKey': $('#APIKey').val(),
                'vendorId': $('#VendorID').val(),
                'productId': $('#productID').val(),
                'countryId': 1,
                'name': $('#name').val(),
                'specifications': $('#specifications').val(),
                'description': $('#description').val(),
                'productCategories': $('#productCategories').val(),
                'similarProducts': null,
                'active': $('#active').val(),
                'brand': $('#brand').val(),
                'stockCount': $('#stockCount').val(),
                'sku': $('#sku').val(),
                'validFrom': null,
                'validTill': null,
                'customerCategory': [0],
                'customerFlags': [0],
                'displayOrder': $('#displayOrder').val(),
                'vendor': $('#VendorID').val(),
                'productImages': JSON.stringify(images),
                'productPrice':{
                    "mrp": $('#mrp').val(),
                    "clientPrice": $('#clientPrice').val(),
                    "currencyCode": $('#currencyCode').val(),
                    "discount": $('#discount').val(),
                    "minimumPoints": $('#minimumPoints').val(),
                    "minimumCash": $('#minimumCash').val(),
                    "basePoints": $('#basePoints').val(),
                    "actualPoints": $('#actualPoints').val(),
                    "ratio": $('#ratio').val(),
                    "productId": $('#productID').val(),
                    "vendor": $('#VendorID').val(),
                },
                'created': date.getTime(),
                'status': $('#status').val(),
                'vendorProductStatus': null,
                'shortDescription': $('#shortDescription').val(),
                'similarProductsByVendor': null,
                'vendorSku': null,
                'partnerId': $('#VendorID').val(),
                'productType': $('#productType').val(),
                'successTempleteId': $('#successTempleteId').val(),
                'failureTempleteId': $('#failureTempleteId').val(),
                'voucherId': $('#voucherId').val(),
                'keyWords': '',
                'keyWords': '',
                'subCustomerCategory': 0,
                'redemptionType': $('#redemptionType').val(),
                'vendorCategory': "",
                'id': $('#VendorID').val()
            };


            console.log(formData);
            // process the form
            $.ajax({
                type        : 'POST',
                url         : url, // the url where we want to POST
                data        : formData,
                dataType    : 'json',
                encode      : true
            })
            // using the done promise callback
                .done(function(data) {

                    console.log(data);

                    if(data.status==0){
//                        $('#alert').removeClass();
//                        $('#alert').addClass('alert alert-success');
//                        $('#message').text(data.statusDesc);

                        localStorage.setItem('messageType', 'success');
                        localStorage.setItem('messageText', 'You have successfully added a new Product');


                        var url = "<?=base_url().'admin/products'?>";
                        window.location.href = url;
                    }
                    if(data.status==1){
                        $('#alert').removeClass();
                        $('#alert').addClass('alert alert-danger');
                        $('#message').text(data.statusDesc);
                    }
                });

            $("#addProduct")[0].reset();
//            // stop the form from submitting the normal way and refreshing the page
//            return false;
            event.preventDefault();
        });


        var imageUpload = "<?php echo base_url()?>/admin/uploadImage/product";
        $("#image").change(function() {

            var files = $('#image')[0].files;
            var error = '';
            var form_data = new FormData();
            form_data.append("alt", $("#name").val());
            form_data.append("productId", $("#productID").val());
            form_data.append("vendor", $("#VendorID").val());
            for(var count = 0; count<files.length; count++)
            {
                var name = files[count].name;
                var extension = name.split('.').pop().toLowerCase();
                if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)
                {
                    error += "Invalid " + count + " Image File"
                }
                else
                {
                    form_data.append("files[]", files[count]);
                }
            }
            if(error == '')
            {
                $.ajax({
                    url: imageUpload,
                    method:"POST",
                    data:form_data,
                    contentType:false,
                    cache:false,
                    processData:false,
                    beforeSend:function()
                    {
//                        $('#uploaded_images').html("<label class='text-success'>Uploading...</label>");
                    },
                    success:function(data)
                    {
//                        console.log(data.uploadLink);

                        if(images[0].url==null)
                        {
                            images = data.uploadLink;
                        }else{
                            images = images.filter(function (el) {
                                return el != null;
                            });

                            var setImages = data.uploadLink;
                            images = images.concat(setImages);
                        }

                        console.log(images);

                        $("#setImage").text('');

                        for(i=0; i<images.length; i++)
                        {
//                            var deleteImage = "<?//=base_url().'delete_image/product/'?>//";
//                            deleteImage = deleteImage+dataSet[i].productImageId;
                            $("#setImage").append(
                                "<div style='width: 130px; float: left; margin: 5px; overflow: hidden;'>" +
                                "<img width='100px'   style='margin: 10px;' src='"+images[i].url+"' /><a href=\"#\" class=\"removeImg\" data-item='"+images[i].url+"' data-value='"+i+"' style=\"top: 0; background-color: #0a0c0a; color: #fff; border-radius:50px; font-size:0.9em; padding: 0 0.3em 0; text-align:center; margin-left: -20px; vertical-align: top; text-decoration: none; cursor:pointer;\" > X </a> " +
                                "</div>"
                            )
                        }
                    }
                })
            }
            else
            {
                alert(error);
            }
        });

        var productList = "<?=base_url().'admin/getProducts'?>";
        var editURL = "<?=base_url().'admin/edit_product/'?>";
        var table = $('#productList').DataTable({
                "processing": true,
                "serverSide": false,
                "ajax":{
                    url  : productList, // json datasource
                    type : "post",
                    "dataSrc": "values"
//                    success: function (res) {
//                        data = res.values;
//
//                        $("#productList").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
//                    },// method  , by default get
//                    error: function(){  // error handling
//                        $("#productList").append('<tbody class="employee-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');


                    },
                    "columns": [
                        { "data": "id" },
                        { "data": "name" },
                        { "data": "shortDescription" },
                        { "data": "stockCount" },
                        { "data": "brand" }
                    ],
                    "columnDefs": [
                        {
                            "targets": 5,
                            "render": function ( data, type, row ) {
                                return "<a href='"+editURL+row.id+"' class='btn btn-primary'> <i class='fa fa-edit'></i> Edit </a>";
                            }
                        }
                    ]

        });


        var messageText = localStorage.getItem("messageText");
        var messageType = localStorage.getItem("messageType");
        if( messageText !='' && messageType !='')
        {
            $('#alert').removeClass('hidden').addClass('alert');
            $('#alert').addClass('alert-success');
            $('#message').text(messageText);

            localStorage.setItem('messageText', '');
            localStorage.setItem('messageType', '');

        }



        $('#setImage').on('click', '.removeImg', function() {

            var i = $( this ).data('value');
            images.splice(-i,1);
            $(this).parent().hide();
            console.log(images);


            if(images.length==0)
            {
                images.push(obj);
            }

            var data = {
                'url': $(this).data('item')
            };

            var removeUpload = "<?=base_url().'admin/removeImage'?>";
            $.ajax({
                url: removeUpload,
                method:"POST",
                data: data,
                success: function(resultData) { console.log("Save Complete") }
            })

        });
    });



</script>
