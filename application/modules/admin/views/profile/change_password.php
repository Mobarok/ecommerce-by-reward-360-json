<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/6/2019
 * Time: 5:25 PM
 */
?>

<!-- START Add User CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="float-left">
                    <h1 class="title">Change Password</h1>   
                    <?php $msg=  $this->session->flashdata('msg'); ?>
                   <span class="text-success"><?= $msg; ?></span>
                </div>

                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>e
                            <a href=""><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="eco-users.html">Users</a>
                        </li>
                        <li class="active">
                            <strong>Add User</strong>
                        </li>
                    </ol>
                </div>
                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="eco-users.html">Users</a>
                        </li>
                        <li class="active">
                            <strong>Add User</strong>
                        </li>
                    </ol>
                </div>

            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xl-12 col-lg-12 col-12 col-md-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">Password Info</h2>
                    <div class="actions panel_actions float-right">
                        <i class="box_toggle fa fa-chevron-down"></i>
                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                        <i class="box_close fa fa-times"></i>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <form action ="<?= base_url(); ?>admin/update_password" method="post">
                            <div class="col-xl-8 col-lg-8 col-md-9 col-12">
                                <div class="form-group">
                                    <label class="form-label" for="old_password">Current Password</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="password" placeholder="Enter Your Current Password Here" value="" class="form-control" name="old_password" id="old_password-2">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="new_password">New Password</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="password"  placeholder="Enter Your New Password Here" value="" class="form-control" name="new_password" id="new_password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="new_password">Confirm New Password</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="password"  placeholder="Please Retype Your New Password Here" value="" class="form-control" name="confirm_password" id="confirm_password">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 padding-bottom-30">
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Save</button>

                                     <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)">  
                                </div>
                            </div>
                    </div>
                    </form>
                </div>


        </div>
    </section></div>
</section>
</section>
<!-- END CONTENT -->

