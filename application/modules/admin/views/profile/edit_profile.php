<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/6/2019
 * Time: 5:10 PM
 */
?>

<!-- Custom Style for input field -->
<style>
    input
    {
        /* background-color: #E8E8E8;
        border: none !important; */
        padding-left: 10px;
    }
</style>

<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="text-center">
                    <h1 class="title">Edit Profile</h1>                            
                </div>

                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="ui-pricing.html">Pages</a>
                        </li>
                        <li class="active">
                            <strong>Profile</strong>
                        </li>
                    </ol>
                </div>

            </div>
        </div>
        <div class="clearfix"></div>


        <div class="col-xl-12">
            <section class="box nobox">
                <div class="content-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="uprofile-image">
                                <?php
                                if( !empty($user['image'])) {
                                    ?>
                                    <img src="<?=$user['image']?>" class="img-fluid">
                                    <?php
                                }else{ ?>
                                    <img src="<?= base_url() ?>admin-assets/data/profile/user.png" class="img-fluid">
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="uprofile-name text-capital">
                                <h3>
                                    <span><?= $user['username']; ?></span>
                                    <!-- Available statuses: online, idle, busy, away and offline -->
                                    <span class="uprofile-status online"></span>
                                </h3>
                                <p class="uprofile-title"><?=$user['role']?></p>
                            </div>


                            <form action ="<?= base_url();?>admin/update_profile/<?=$user['id']?>" method="post" enctype="multipart/form-data">

                            <div class="uprofile-info">

                                <div class="text-center">
                                    <table class="display table table-hover table-condensed form-table" cellspacing="0" width="100%">
                                        <thead>
                                        <tr >
                                            <td colspan="2">
                                            <?php
                                            $message = $this->session->userdata('message');
                                            if( isset($message) ){
                                                $type = $message['type']
                                                ?>
                                                <div class="alert <?php echo $type == 'danger' ? 'alert-danger' : 'alert-success'; ?> ">
                                                    <button class="close" data-close="alert"></button>
                                                    <span><?=$message['text'];?></span>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            </td>
                                        </tr>
                                            <tr>
                                                <td colspan="2"  class="text-center" style="font-size: 22px;font-weight: bold;">Edit User Profile</td>
                                            </tr>
                                        </thead>
                                        <tbody class="form-edit">
                                        <form action ="<?= base_url('admin/update_profile') ?>" enctype="multipart/form-data" method="post">

                                        <tr>
                                                <th class="profile-th">Full Name</th>
                                                <td><input type="text" name="full_name" value="<?=$user['full_name']?>"></td>
                                            </tr>
                                            <tr>
                                                <th class="profile-th">Phone</th>
                                                <td><input type="text" name="phone" value="<?=$user['phone']?>"></td>
                                            </tr>
                                            <tr>
                                                <th class="profile-th">Address</th>
                                                <td><input type="text" name="address" value="<?=$user['address']?>"></td>
                                            </tr>
                                            <tr>
                                                <th class="profile-th">Image</th>
                                                <td><input type="file" name="image"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                            <div class="text-right padding-bottom-20">

                                <button type="submit" class="btn btn-primary"> Update Profile</button>
                                <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)">
                            </div>


                            </form>

                        </div>
                    </div>
                </div>
            </section></div>


    </section>
</section>
<!-- END CONTENT -->
          
