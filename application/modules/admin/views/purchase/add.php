
<style type="text/css">
    td{padding: 10px 5px};
</style>
<!-- START Add Purchase CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="float-left">
                    <h1 class="title">Add Purchase</h1>
                   <?php $msg=  $this->session->flashdata('msg'); ?>
                   <span class="text-success"><?= $msg; ?></span>
                </div>

                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>
                            <a href=""><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="">Products</a>
                        </li>
                        <li class="active">
                            <strong>Add Purchase</strong>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xl-12 col-lg-12 col-12 col-md-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">Purchase Info</h2>
                    <div class="actions panel_actions float-right">
                        <i class="box_toggle fa fa-chevron-down"></i>
                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                        <i class="box_close fa fa-times"></i>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <form action ="<?= base_url('admin/create_purchase') ?>" method="post">

                            <table class="table" >

                                <tbody id="add-purchase-row">
                                <tr>
                                    <td width="150px">
                                        <input type="text"  class="form-control" id="product_name" name="product_name[]" placeholder="Product Name" required>
                                    </td>
                                    <td  width="150px">
                                        <input type="text"  class="form-control" name="product_id[]" id="product_id" placeholder="Product ID" required>
                                    </td>
                                    <td  width="150px">
                                        <!--<textarea name="description[]" class="form-control autogrow" cols="5" id="description"></textarea>-->
                                         <input type="text"  class="form-control" name="description[]" id="description" placeholder="Description">
                                    </td>
                                    <td width="200px">
                                        <select class="form-control selectpicker" placeholder="Please select Category" name="product_category[]" id="product_category" data-live-search="true">
                                            <?php
                                            foreach ($categories as $category)
                                            { ?>
                                                <option data-tokens="<?= $category['categoryId'] ?>" value="<?= $category['categoryId'] ?>"><?= $category['name'] ?></option>
                                            <?php  }
                                            ?>
                                        </select>
                                    </td>
                                    <td width="200px">
                                        <select class="form-control selectpicker" data-live-search="true" name="brand[]" id="brand">
                                            <?php
                                            foreach ($brands as $brand)
                                            { ?>
                                                <option data-tokens="<?= $brand['name'] ?>" value="<?= $brand['name'] ?>"><?= $brand['name'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td width="100px" >
                                        <input type="number" min="0" onblur="totalPrice()" name="quantity[]"  class="form-control" id="quantity" placeholder="Qty" required>
                                    </td>
                                    <td width="150px" >
                                        <input type="number" min="0" placeholder="Price" class="form-control" name="product_price[]" id="product_price" required>
                                    </td>
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <td  colspan="7" class="text-left">
                                            <button class="btn btn-primary add_row" type="button"> <i class="fa fa-plus"></i> Add New Purchase</button>
                                        </td>
<!--                                        <td  class="text-right">-->
<!--                                            <label class="text-bold" for="vendor"> Total Price</label>-->
<!--                                        </td>-->
<!--                                        <td colspan="2">-->
<!--                                            <input type="number"  readonly name="total_price" min="0" class="form-control" id="total_price" >-->
<!--                                        </td>-->
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="col-sm-12 col-xl-6 col-lg-6 col-md-6 col-6">
                                <div class="form-group">
                                    <label class="form-label" for="vendor">Vendor</label>
                                    <span class="desc"></span>
                                    <span class='text-danger'><?php echo form_error('vendor'); ?></span>
                                    <div class="controls">
                                        <input type="text" name="vendor" class="form-control" id="vendor" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="remark">Remark</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" name="remark" class="form-control" id="remark">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-6 col-lg-6 col-md-6 col-6">
                                <div class="form-group">
                                    <label class="form-label" for="payment">Payment</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="number" name="payment" min="0" class="form-control" id="payment" >
                                    </div>
                                </div>
<!--                                <div class="form-group">-->
<!--                                    <label class="form-label" for="due">Due</label>-->
<!--                                    <span class="desc"></span>-->
<!--                                    <div class="controls">-->
<!--                                        <input type="number" name="due" min="0" class="form-control" id="due">-->
<!--                                    </div>-->
<!--                                </div>-->
                                <div class="form-group">
                                    <label class="form-label" for="status">Status</label>
                                    <span class="desc"></span>
                                    <select class="form-control cus-form-control" name="status" id="status">
                                        <option data-tokens="Paid" value="0">Paid</option>
                                        <option data-tokens="Unpaid" value="1">Unpaid</option>
                                        <option data-tokens="Cancel" value="2">Cancel</option>
                                    </select>
                                </div>
                                
                                
                            </div>

                            <div class="clear"></div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 padding-bottom-30">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 padding-bottom-30">
                                        <div class="text-right">
                                            <input type="submit" class="btn btn-primary" value="Save" />
                                            <!--                                    <button type="reset" class="btn btn-warning ">Reset</button>-->
                                            <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>


                </div>
            </section>
        </div>
    </section>
</section>


<!-- Total Price Calculate From Form input -->
<script>

    totalPrice = function()
    {
        var price = document.getElementById('product_price').value;
        var quantity = document.getElementById('quantity').value; 
        document.getElementById('total_price').value = parseInt(price)*parseInt(quantity);

    }


</script>
