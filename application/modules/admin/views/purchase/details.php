<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/20/2019
 * Time: 12:45 PM
 */
?>

<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="float-left">
                    <h1 class="title">Purchase Details</h1>
                </div>

<!--                <div class="float-right d-none">-->
<!--                    <ol class="breadcrumb">-->
<!--                        <li>-->
<!--                            <a href=""><i class="fa fa-home"></i>Home</a>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="">Products</a>-->
<!--                        </li>-->
<!--                        <li class="active">-->
<!--                            <strong>Update Product Stock</strong>-->
<!--                        </li>-->
<!--                    </ol>-->
<!--                </div>-->
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xl-12 col-lg-12 col-12 col-md-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left" style="text-transform: none;">Detailed Information on Purchase</h2>
                    <div class="actions panel_actions float-right">
                        <i class="box_toggle fa fa-chevron-down"></i>
                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                        <i class="box_close fa fa-times"></i>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <table class="table text-left details">
                            <tr>
                                <th>Vendor Name</th>
                                <td><?=$result['vendor'];?></td>
                                <?php
                                if ($result['remark']){
                                ?>
                                <th>Remark</th>
                                <td><?=$result['remark'];?></td>
                                <?php
                                }
                                ?>
                                <td  colspan="<?= ($result['remark']!='' ?  '5' :  '7') ?>"></td>
                            </tr>

                            <?php
                            foreach ($purchase_details as $key => $purchase) {

                                if ($key == 0){
                            ?>

                                    <tr class="text-center">
                                        <th>
                                            #SL
                                        </th>
                                        <th>Product Name</th>
                                        <th>Product ID</th>
                                        <th>Product Category</th>
                                        <th>Brand</th>
                                        <th>Description</th>
                                        <th>Quantity</th>
                                        <th class="text-center">Product Price (RM)</th>
                                        <th class="text-center">Total Price (RM)</th>
                                    </tr>
                                    <?php
                            }
                            ?>
                                <tr class="text-center">
                                    <td>
                                       <?=$key+1?>
                                    </td>
                                    <td> <?=$purchase['product_name'];?></td>
                                    <td><?=$purchase['product_id'];?></td>
                                    <td> <?=$purchase['categoryName'];?></td>
                                    <td><?=$purchase['brand'];?></td>
                                    <td ><?=$purchase['description'];?></td>
                                    <td> <?=$purchase['quantity'];?> </td>

                                    <td class="text-center"><?=number_format((float)$purchase['product_price'], 2, '.', ',');?></td>
                                    <td class="text-right"><?=number_format((float)$purchase['total'], 2, '.', ',');?></td>
                                </tr>
                            <?php

                            }
                            ?>
                            <tr>
                                <td colspan="9" style="height: 40px;"></td>
                            </tr>
                            <tr class="totalLine">
                                <th colspan="7"></th>
                                <th>Subtotal Price (RM)</th>
                                <td class="text-right bold" ><?=number_format((float)$result['total_price'], 2, '.', ',');?></td>
                            </tr>

                            <tr>
                                <th colspan="7" style="border: none;"></th>
                                <th  style="border: none;">Payment (RM)</th>
                                <td class="text-right"  style="border: none;"><?=number_format((float)$result['payment'], 2, '.', ',');?></td>
                            </tr>

                            <tr>
                                <th colspan="7"></th>
                                <th>Due (RM)</th>
                                <td class="text-right bold"><?=number_format((float)$result['due'], 2, '.', ',');?></td>
                            </tr>
                        </table>
                    </div>

                    <div class="text-right padding-bottom-20">

                        <a href="<?=base_url().'admin/edit_purchase/'.$result['purchase_id'];?>" class="btn btn-primary"> <i class="fa fa-edit"></i> Edit </a>

                             <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)">

                    </div>
                </div>

            </section>


        </div>
    </section>
</section>
