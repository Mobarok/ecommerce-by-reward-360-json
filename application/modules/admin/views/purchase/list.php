
<!-- dataTable fixed column CSS -->
<style type="text/css">
        th, td { white-space: nowrap; }
            div.dataTables_wrapper {
                margin: 0 auto;
            }
            
            div.container {
                width: 80%;
            }
</style>


<!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="float-left">
                    <h1 class="title">Purchase List</h1>
                </div>

                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>
                            <a href="#"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="#">Purchases</a>
                        </li>
                        <li class="active">
                            <strong>Purchase List</strong>
                        </li>
                    </ol>
                </div>

            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-lg-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">All Purchases</h2>
                    <div class="actions panel_actions float-right">
                        <i class="box_toggle fa fa-chevron-down"></i>
                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                        <i class="box_close fa fa-times"></i>
                    </div>
                </header>
                <div class="content-body">    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <?php
                            $message = $this->session->userdata('message');
                            if( isset($message) ){
                                $type = $message['type']
                                ?>
                                <div class="alert <?php echo $type == 'danger' ? 'alert-danger' : 'alert-success'; ?> ">
                                    <button class="close" data-close="alert"></button>
                                    <span><?=$message['text'];?></span>
                                </div>
                                <?php
                            }
                            ?>

                            <!-- ********************************************** -->



                            <table id="purchase_list" class="display table table-hover table-condensed" cellspacing="0" width="100%">
                                <thead>

                                        <tr>
                                        <th width="30px">#SL</th>
                                        <th>Product Name</th>
                                        <th>Product ID</th>
                                        <!--<th>Description</th>-->
                                        <th>Quantity</th>
                                        <!-- <th>Product Category</th> -->
                                        <!--<th>Brand</th> -->
                                        <th>Price (RM)</th>
                                        <!--<th>Vendor</th> -->
                                        <!--<th>Remark</th>-->
                                        <th>Total Price (RM) </th>
                                        <!--<th>Payment</th> -->
                                        <!--<th>Due</th> -->
                                        <th>Status</th> 
                                        <th class="text-center">Manage</th>
                                    </tr>
                                </thead>

                                <tbody>
                            <?php 
                             $last_id = $count = 0;
                             $rowspan = '';
                            foreach($purchases as $key => $purchase ){
                            ?>

                                    <tr>
                                    <?php
                                     #endregion

//                                     if($last_id = $purchase['main_purchase_id']){
//                                         $count = $count+1;
//                                        $rowspan = '1';
//                                     }else{
//                                         $count = 0;
//                                     }
//
//                                     $last_id = $purchase['purchase_id'];
//
//                                     if($rowspan=='1')
//                                     {
//                                        echo "<td rowspan='".$count."'>1</td>";
//                                     }else{
//                                        echo "<td rowspan='".$count."'>1</td>";
//
//                                     }
                                    ?>
                                        
                                        <td width="30px"><?= $key+1 ?></td>
                                        <td width="150px"><?= $purchase['product_name'] ?></td>
                                        <td><?= $purchase['product_id'] ?></td>
                                        <!--<td><?= $purchase['description'] ?></td>-->
                                        <td><?= $purchase['quantity'] ?></td>
                                        <!-- <td><?php
                                        // echo $purchase['product_category_id'] 
                                        ?></td> -->
                                        <!--<td><?= $purchase['brand'] ?></td>-->
                                        <td><?= number_format((float)$purchase['product_price'], 2, '.', ',');?></td>
                                        <!--<td><?= $purchase['vendor'] ?></td>-->
                                        <!--<td><?= $purchase['remark'] ?></td>-->
                                        <td><?= number_format((float)$purchase['total_price'], 2, '.', ',');?></td>
                                        <!--<td><?= $purchase['payment'] ?></td>-->
                                        <!--<td><?= $purchase['due'] ?></td>-->
                                        <td>
                                        <?php 
                                            if($purchase['status'] == 0)
                                            {
                                                echo "Paid";
                                            }
                                               
                                            elseif($purchase['status'] == 1)
                                            { 
                                                   echo "Unpaid";
                                            }
                                            elseif($purchase['status'] == 2)
                                            {
                                                echo "Cancel";
                                            }
                                        ?>                                        
                                        </td>
                                        <td class="text-center">
                                            <a href="<?= base_url(); ?>admin/purchase_details/<?= $purchase['purchase_id']; ?>" class="btn btn-primary"> <i class="fa fa-eye"></i> View </a>
                                            <a href="<?= base_url(); ?>admin/delete_purchase/<?= $purchase['purchase_id']; ?>" onclick="return confirm('Are you Sure to Delete? ');" class="btn btn-danger"> <i class="fa fa-trash"></i> Delete </a>
                                        </td>
                                    </tr>
        <?php } ?>
                                </tbody>
                            </table>

                                    
                            <!-- ********************************************** -->




                        </div>
                    </div>

            <div class="mb-4 mt-3">

               <span style="float:right">
                  
                     <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)">  
               </span>

            </div>

                </div>
            </section>
        </div>
    </section>
</section>
<!-- END CONTENT -->
