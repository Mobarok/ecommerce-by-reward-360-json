
<!-- Script for purchase list column fixed -->

<script type="text/javascript">

$(document).ready(function(){

   var table = $('#purchase_list').DataTable({
		columnDefs: [
            { width: 30, targets: 0 }
        ],
        "lengthMenu": [[10, 25, 50, 100,-1], [10, 25, 50,100, "All"]],
        fixedColumns: true
    });


    var categories = '';
    var brands = '';
    <?php
        if(isset($categories)){

        foreach ($categories as $category)
        {
        $categoryId = $category["categoryId"];
        $categoryName = $category["name"];
        ?>

        categories += '<option data-tokens="<?=$categoryId?>" value="<?=$categoryId?>"><?=$categoryName ?></option>';
        <?php  }

    }
    ?>

        <?php

    if(isset($brands)){

    foreach ($brands as $brand)
        {
            $brandName = $brand['name'];
            $brandId = $brand['id'];
            ?>
        brands += '<option data-tokens="<?= $brandId?>" value="<?=$brandName?>"><?=$brandName?></option>';
    <?php }
    }
    ?>


    $( ".add_row" ).click(function() {

        var addRow = '<tr><td width="150px"><input type="text"  class="form-control" id="product_name" name="product_name[]" placeholder="Product Name" required></td><td  width="150px"><input type="text"  class="form-control" name="product_id[]" id="product_id" placeholder="Product ID" required></td><td width="150px"><input type="text"  class="form-control" name="description[]" id="description" placeholder="Description"></td><td width="200px"><select class="form-control selectpicker" placeholder="Please select Category" name="product_category[]" id="product_category" data-live-search="true">'+categories+ '</select></td><td width="200px"><select class="form-control selectpicker" data-live-search="true" name="brand[]" id="brand">'+brands+'</select></td><td width="100px" ><input type="number" min="0" onblur="totalPrice()" name="quantity[]"  class="form-control" id="quantity" placeholder="Qty" required></td><td width="150px" ><input type="number" min="0" placeholder="Price" class="form-control" name="product_price[]" id="product_price" required></td>';

        $('#add-purchase-row tr:last').after(addRow);

        $('.selectpicker').selectpicker('refresh');
    });


    $('.selectpicker').selectpicker('refresh');
});




</script>