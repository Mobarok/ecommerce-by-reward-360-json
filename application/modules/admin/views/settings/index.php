<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/12/2019
 * Time: 2:32 PM
 */
?>
<style>
    input
    {
        /* background-color: #E8E8E8;
        border: none !important; */
        padding-left: 10px;
    }
</style>

<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="text-left">
                    <h1 class="title">Edit Settings</h1>
                </div>

                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="ui-pricing.html">Pages</a>
                        </li>
                        <li class="active">
                            <strong>Settings</strong>
                        </li>
                    </ol>
                </div>

            </div>
        </div>
        <div class="clearfix"></div>


        <div class="col-xl-12">
            <section class="box nobox">
                <div class="content-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-12">

                            <form action ="<?= base_url();?>admin/update_settings" method="post" enctype="multipart/form-data">

                            <div class="uprofile-info">
                                <div class="text-left">
                                        <table class="display table table-hover table-condensed form-table" cellspacing="0" width="100%">
                                            <thead>
                                            <td colspan="2">
                                                <?php
                                                $message = $this->session->userdata('message');
                                                if( isset($message) ){
                                                    $type = $message['type']
                                                    ?>
                                                    <div class="alert <?php echo $type == 'danger' ? 'alert-danger' : 'alert-success'; ?> ">
                                                        <button class="close" data-close="alert"></button>
                                                        <span><?=$message['text'];?></span>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                            </thead>
                                            <tbody class="form-edit">
                                            <tr>
                                                <th class="profile-th">Site Name</th>
                                                <td><input type="text" name="site_name" value="<?=$this->config->item('siteName');?>"></td>
                                            </tr>
                                            <tr>
                                                <th class="profile-th"> Address</th>
                                                <td><input type="text" name="address" value="<?=$this->config->item('address');?>"></td>
                                            </tr>
                                            <tr>
                                                <th class="profile-th">Logo Image</th>
                                                <td>
                                                    <input type="file" name="logo">

                                                    <?php
                                                    if(!empty($this->config->item('logo')))
                                                    {
                                                        ?>
                                                        <img width="100px" src="<?=$this->config->item('logo');?>"/>
                                                    <?php
                                                    }else{
                                                       ?>
                                                        <img width="100px" src="<?=base_url().'admin-assets/assets/images/jcms.png';?>"/>
                                                    <?php
                                                    }
                                                    ?>


                                                </td>
                                            </tr>
                                            <tr>
                                                <th class="profile-th">Favicon</th>
                                                <td>
                                                    <input type="file" name="favicon">

                                                    <?php
                                                    if(!empty($this->config->item('favicon')))
                                                    {
                                                        ?>
                                                        <img width="40px" src="<?=$this->config->item('favicon');?>"/>
                                                        <?php
                                                    }else{
                                                        ?>
                                                        <img width="40px" src="<?=base_url().'admin-assets/assets/images/favicon-jcms.png';?>"/>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                </div>
                            </div>
                            <div class="text-right">

                                <input type="submit" class="btn btn-primary" value="Update Settings">
                                <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)">
                            </div>

                            </form>

                        </div>
                    </div>
                </div>
            </section></div>


    </section>
</section>
