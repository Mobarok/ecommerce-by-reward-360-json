<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/19/2019
 * Time: 7:40 PM
 */
?>
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="float-left">
                    <h1 class="title">Shipment Details</h1>
                </div>

                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>
                            <a href=""><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="">Products</a>
                        </li>
                        <li class="active">
                            <strong>Update Product Stock</strong>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xl-12 col-lg-12 col-12 col-md-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left" style="text-transform: none;">Detailed Information on Shipment</h2>
                    <div class="actions panel_actions float-right">
                        <i class="box_toggle fa fa-chevron-down"></i>
                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                        <i class="box_close fa fa-times"></i>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <table class="table text-left details">
                            <tr>
                                <th>Order Reference</th>
                                <td>CC00010910</td>
                                <th>Product ID</th>
                                <td>KI09</td>
                            </tr>
                            <tr>
                                <th>Customer ID</th>
                                <td>10</td>
                                <th>Product Category</th>
                                <td>8</td>

                            </tr>
                            <tr>
                                <th>Product Name</th>
                                <td>Wildcraft Pencil Pouch</td>
                                <th>Quantity</th>
                                <td>20</td>
                            </tr>
                            <tr>
                                <th>Amount Paid (RM)</th>
                                <td>100,000.00</td>
                                <th>Status</th>
                                <td>Paid</td>
                            </tr>
                            <tr>

                                <th>Courier Name</th>
                                <td>Blue Dart</td>
                                <th>Courier Reference Number</th>
                                <td>AWB</td>
                            </tr>
                            <tr>

                                <th>Order Date</th>
                                <td>11/03/2018</td>
                                <th>Dispatched Date</th>
                                <td>12/03/2018</td>
                            </tr>
                            <tr>

                                <th>Delivery Date</th>
                                <td>19/03/2018</td>
                                <th>Order Remark</th>
                                <td>I need good quality</td>
                            </tr>
                        </table>
                    </div>

                    <div class="row text-right" >

                        <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)">
                    </div>
                </div>

            </section>


        </div>
    </section>
</section>
