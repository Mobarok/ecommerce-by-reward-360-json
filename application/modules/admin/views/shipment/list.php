<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/7/2019
 * Time: 10:52 PM
 */


?>


<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="float-left">
                    <h1 class="title">Shipment Schedule</h1>                            </div>

                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="eco-users.html">Users</a>
                        </li>
                        <li class="active">
                            <strong>All Users</strong>
                        </li>
                    </ol>
                </div>

            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-lg-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">Shipment Details</h2>
                    <div class="actions panel_actions float-right">
                        <i class="box_toggle fa fa-chevron-down"></i>
                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                        <i class="box_close fa fa-times"></i>
                    </div>
                </header>
                <div class="content-body">    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">



                            <!-- ********************************************** -->

                                 <table id="listShipment" class="display table table-hover table-condensed " cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>Order Reference</th>
                                    <th>Customer ID</th>
                                    <th>Product ID</th>
                                    <th>Product Name</th>
                                    <!-- <th>Product Category</th> -->
                                    <th>Quantity</th>
                                    <th>Amount Paid</th>
                                    <!-- <th>Courier Name</th> -->
                                    <!-- <th>Courier Reference Number</th> -->
                                    <!-- <th>Dispatched Date</th> -->
                                    <th>Delivery Date</th>
                                    <th>Manage</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php
                                for($i=2; $i<50; $i++) {
                                    ?>
                                    <tr>
                                        <td>CC00<?=$i?>09<?=$i?></td>
                                        <td>CUS<?=$i?>09<?=$i?></td>
                                        <td>KI<?=$i?>09</td>
                                        <td>Wildcraft Pencil Pouch</td>
                                        <!-- <td>8</td> -->
                                        <td><?=$i?><?=$i?></td>
                                        <td><?php if($i%2==0) { echo "Paid"; }else{ echo "Unpaid";  } ?></td>
                                        <!-- <td>Blue Dart</td> -->
                                        <!-- <td>AWB</td> -->
                                        <!-- <td>12/03/2018</td> -->
                                        <td>19/03/2018</td>
                                        <td>
                                            <a href="<?=base_url().'admin/shipment_details';?>" class="btn btn-primary "><i class="fa fa-eye"></i> View </a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>

                            <!-- ********************************************** -->




                        </div>
                    </div>

               <div class="mb-4 mt-3">

               <span style="float:right">
               <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)">
               </span>

               </div>

                </div>
            </section></div>
    </section>
</section>
<!-- END CONTENT -->

