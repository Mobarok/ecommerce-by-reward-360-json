<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/7/2019
 * Time: 10:53 PM
 */
?>


<script type="text/javascript">
    $.noConflict();
    jQuery(document).ready(function($){
        var table = $('#listShipment').DataTable({
            columnDefs: [
                { width: 200, targets: 0 }
            ],
        "lengthMenu": [[10, 25, 50, 100,-1], [10, 25, 50,100, "All"]],
            fixedColumns: true
        } );
    });
</script>
