<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/8/2019
 * Time: 5:10 PM
 */
?>

<style type="text/css">
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }

    div.container {
        width: 80%;
    }
</style>
