<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/7/2019
 * Time: 10:34 PM
 */
?>

<style type="text/css">
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }

    div.container {
        width: 80%;
    }
</style>


<!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="float-left">
                    <h1 class="title">All Stock</h1>                            </div>

                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>
                            <a href="#"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="#">Stocks</a>
                        </li>
                        <li class="active">
                            <strong>All Stocks</strong>
                        </li>
                    </ol>
                </div>

            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-lg-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">Stock Details</h2>
                    <div class="actions panel_actions float-right">
                        <i class="box_toggle fa fa-chevron-down"></i>
                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                        <i class="box_close fa fa-times"></i>
                    </div>
                </header>
                <div class="content-body">    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">



                            <!-- ********************************************** -->

                            <div class="text-center">
                                <table id="stockList" class="display table table-hover table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Product ID</th>
                                            <th>Product Name</th>
                                            <th>Vendor</th>
                                            <th>Stock</th>
                                            <th>Manage</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                    <?php
//                                    for($i=0; $i<50; $i++) {
//                                        ?>
<!--                                        <tr>-->
<!--                                            <td> SAF1--><?//=$i?><!--0</td>-->
<!--                                            <td>9--><?//=$i?><!--</td>-->
<!--                                            <td>-->
<!--                                                <a href="--><?//=base_url()?><!--admin/edit_stock" class="btn btn-primary"> <i class="fa fa-edit"></i> Edit</a>-->
<!--                                            </td>-->
<!--                                        </tr>-->
<!--                                        --><?php
//                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <!-- ********************************************** -->




                        </div>
                    </div>
                    <div class="row text-right">
                        <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)">  
                    </div>
                </div>
            </section></div>
    </section>
</section>
<!-- END CONTENT -->

