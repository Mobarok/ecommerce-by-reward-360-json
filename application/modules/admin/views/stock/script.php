<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/7/2019
 * Time: 10:41 PM
 */
?>



<script type="text/javascript">

    $(document).ready(function() {

        var stockList = "<?=base_url().'admin/getStocks'?>";
        var editURL = "<?=base_url().'admin/edit_stock/'?>";

        var dataSrc = '';

        $.ajax({
            url: stockList,
            type: 'get',
            success: function (data) {

               
                var obj =  JSON.parse(data.value);
                $.each( obj, function( key, value ) {
               
                    var pID = key.split(':');
//                    console.log( key + ": " + value );
                $('#stockList').dataTable().fnDestroy();
                    $('#stockList tbody').append("<tr>" +
                        "<td>"+pID[0]+"</td>" +
                        "<td>"+pID[1]+"</td>" +
                         "<td>"+pID[2]+"</td>" +
                        "<td>"+value+"</td>" +
                        "<td>" +
                            "<a class='btn btn-primary' href='"+editURL+pID[0]+"/"+value+"'>" +
                                "<i class='fa fa-edit'> </i> Edit"
                            +"</a></td>" +
                        +"</tr>");


                    var table = $('#stockList').DataTable({
                        columnDefs: [
                            { width: 200, targets: 0 }
                        ],
                    "lengthMenu": [[10, 25, 50, 100,-1], [10, 25, 50,100, "All"]],
                        fixedColumns: true
                    } );

                     
                });


            }
        });


        



//        $('#stockList').DataTable({
//            "processing": true,
//            "serverSide": false
//        });
//        var table = $('#stockList').DataTable({
//            "processing": true,
//            "serverSide": false,
//            "ajax":{
//                url  : stockList, // json datasource
//                type : "post",
//                "dataSrc": function ( data ) {
//                    //Make your callback here.
////                    alert("Done!");
//                    var obj =  JSON.parse(data.values);
//                    $.each( obj, function( key, value ) {
//                        console.log( key + ": " + value );
//                        $('#stockList tbody').append("<tr><td>"+key+"</td><td>"+value+"</td></tr>")
//                    });
//                }
//            }
//
//        });


        var updateUrl = '<?php echo base_url()."admin/update_stock/";?>';
        $('#editStock').submit(function(event) {

            var productID = $("#productID").val();
            var qty = $("#qty").val();
            // process the form
            $.ajax({
                type        : 'get',
                url         : updateUrl+"/"+productID+"/"+qty, // the url where we want to POST
                dataType    : 'json',
                encode      : true
            })
            // using the done promise callback
                .done(function(data) {

                    if(data==true){
                        var redirectURL = '<?=base_url()."admin/edit_stock/"?>'+productID+"/"+qty;
                        window.location.href = redirectURL;
                        $('#alert').removeClass();
                        $('#alert').addClass('alert alert-success');
                        $('#message').text('Successfully Stock Updated');
                    }
                    if(data==false){
                        $('#alert').removeClass();
                        $('#alert').addClass('alert alert-danger');
                        $('#message').text('Unable to update Stock');
                    }
                });
//            // stop the form from submitting the normal way and refreshing the page
            event.preventDefault();
        });

    });
</script>
