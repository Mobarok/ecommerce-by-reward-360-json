<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/7/2019
 * Time: 10:34 PM
 */
?>
<!-- START Add User CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="float-left">
                    <h1 class="title">Update Product Stock</h1>                            </div>

                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>
                            <a href=""><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="">Products</a>
                        </li>
                        <li class="active">
                            <strong>Update Product Stock</strong>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xl-12 col-lg-12 col-12 col-md-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">Stock Info</h2>
                    <div class="actions panel_actions float-right">
                        <i class="box_toggle fa fa-chevron-down"></i>
                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                        <i class="box_close fa fa-times"></i>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <form action="" id="editStock" method="post">
                            <div class="col-xl-8 col-lg-8 col-md-9 col-12">

                                <div class="hidden" id="alert">
                                    <button class="close" data-close="alert"></button>
                                    <span id="message"></span>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="name">Product Name</label>
                                    <span class="desc"></span>
                                    <div class="controls">

                                         <select class="form-control selectpicker" placeholder="Please select Category" name="product_category[]" id="productID" data-live-search="true">
                                            <?php
                                            foreach ($all_product as $product)
                                            { ?>
                                                <option value="<?= $product['id'] ?>"><?=$product['name'] ?></option>
                                            <?php  }
                                            ?>
                                        </select>

                                        <!-- <input type="text" readonly value="<?=$productId?>" class="form-control" id="productID" placeholder="Product ID"> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="quantity">Quantity</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input min="0" type="number" value="<?=$qty?>" class="form-control" id="qty">
                                    </div>
                                </div>


                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 padding-bottom-30">
                                <div class="text-right">
                                    <input type="submit" class="btn btn-primary" value="Update" />

                                        <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)">  
                                </div>
                            </div>
                    </div>
                    </form>
                </div>


        </div>
    </section></div>
</section>
</section>

