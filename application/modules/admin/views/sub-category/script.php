<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/3/2019
 * Time: 4:49 PM
 */
?>

<script type="application/javascript">
    $(document).ready(function() {
        var date = new Date();
        var url = '<?php echo base_url()."admin/submit_category";?>';
        // process the form

        var images;
        var editImage = "<?php if(isset($result)) { echo $result['cat_featured_image'];}?>";

        if(editImage != null) {
            images = editImage;
        }else {
            images = '';
        }

        $('#addCategory').submit(function(event) {

            // get the form data
            // there are many ways to get this data using jQuery (you can use the class or id also)
            var formData = {
                'APIKey': $('#APIKey').val(),
                'vendorId': $('#VendorID').val(),
                'name': $('#name').val(),
                'countryId': 1,
                'slug': $('#slug').val(),
                'status': $('#status').val(),
                'description': $('#description').val(),
                'displayOrder': $('#displayOrder').val(),
                'cat_featured_image': images,
                'parentId': $('#parentId').val(),
                'created': date.getTime(),
                'updated': null
            };


            // process the form
            $.ajax({
                type        : 'POST',
                url         : url, // the url where we want to POST
                data        : formData,
                dataType    : 'json',
                encode      : true
            })
            // using the done promise callback
                .done(function(data) {

                    if(data==true){
//                        $('#alert').removeClass();
//                        $('#alert').addClass('alert alert-success');
//                        $('#message').text('You have successfully created a new category');

                        localStorage.setItem('messageType', 'success');
                        localStorage.setItem('messageText', 'You have successfully created a new category');


                        var url = "<?=base_url().'admin/subcategories'?>";
                    }
                    if(data==false){
                        $('#alert').removeClass();
                        $('#alert').addClass('alert alert-danger');
                        $('#message').text('Unable to Add Category');
                    }
                });

            $("#addCategory")[0].reset();
//            // stop the form from submitting the normal way and refreshing the page
            event.preventDefault();
        });


        var updateUrl = '<?php echo base_url()."admin/update_category";?>';
        $('#editCategory').submit(function(event) {

            // get the form data
            // there are many ways to get this data using jQuery (you can use the class or id also)
            var formData = {
                'APIKey': $('#APIKey').val(),
                'vendorId': $('#VendorID').val(),
                'name': $('#name').val(),
                'countryId': 1,
                'categoryId': $('#categoryId').val(),
                'status': $('#status').val(),
                'description': $('#description').val(),
                'displayOrder': $('#displayOrder').val(),
                'cat_featured_image': images,
                'parentId': $('#parentId').val(),
                'updated': date.getTime()

            };


            // process the form
            $.ajax({
                type        : 'POST',
                url         : updateUrl, // the url where we want to POST
                data        : formData,
                dataType    : 'json',
                encode      : true
            })
            // using the done promise callback
                .done(function(data) {

                    if(data==true){
//                        $('#alert').removeClass();
//                        $('#alert').addClass('alert alert-success');
//                        $('#message').text('You have successfully updated a category');
                        localStorage.setItem('messageType', 'success');
                        localStorage.setItem('messageText', 'You have successfully updated a  category');


                        var url = "<?=base_url().'admin/subcategories'?>";
                        window.location.href = url;
                    }
                    if(data==false){
                        $('#alert').removeClass();
                        $('#alert').addClass('alert alert-danger');
                        $('#message').text('Unable to update Category');
                    }
                });
//            // stop the form from submitting the normal way and refreshing the page
            event.preventDefault();
        });



        var imageUpload = "<?php echo base_url()?>/admin/uploadImage/subcategory";
        $("#image").change(function() {

            var files = $('#image')[0].files;
            var  input =  this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#setImage').text('');
                    $('#setImage').append('<img src="'+e.target.result+'" width="120px"/> <a href="#" class="removeImg" style="top: 0; background-color: #0a0c0a; color: #fff; border-radius:50px; font-size:0.9em; padding: 0 0.3em 0; text-align:center; margin-left: -20px; vertical-align: top; text-decoration: none; cursor:pointer;" > X </a>');
                }

                reader.readAsDataURL(input.files[0]);
            }

            var error = '';
            var form_data = new FormData();
            for(var count = 0; count<files.length; count++)
            {
                var name = files[count].name;
                var extension = name.split('.').pop().toLowerCase();
                if(jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1)
                {
                    error += "Invalid " + count + " Image File"
                }
                else
                {
                    form_data.append("files[]", files[count]);
                }
            }
            if(error == '')
            {
                $.ajax({
                    url: imageUpload,
                    method:"POST",
                    data:form_data,
                    contentType:false,
                    cache:false,
                    processData:false,
                    beforeSend:function()
                    {
//                        $('#uploaded_images').html("<label class='text-success'>Uploading...</label>");
                    },
                    success:function(data)
                    {
                        images = data.uploadLink;
//                        $('#uploaded_images').html(data);
//                        $('#files').val('');
                    }
                })
            }
            else
            {
                alert(error);
            }
        });

        var messageText = localStorage.getItem("messageText");
        var messageType = localStorage.getItem("messageType");
        if( messageText !='' && messageType !='')
        {
            $('#alert').removeClass('hidden').addClass('alert');
            $('#alert').addClass('alert-success');
            $('#message').text(messageText);

            localStorage.setItem('messageText', '');
            localStorage.setItem('messageType', '');

        }

        var category = [];
        var getCategory = "<?php echo base_url().'/admin/get_category'?>";
        $.ajax({
            url: getCategory,
            context: document.body,
            success: function(data){
                console.log("Default: "+category);
                console.log(data.values);
                category = data.values;
                console.log(category.length);
            }
        });


        var getSubCategory = "<?php echo base_url().'/admin/get_category/sub'?>";
        var editUrl = '<?php echo base_url()."admin/edit_category/"?>';
        var removeUrl = '<?php echo base_url()."admin/delete_category/category/"?>';

        var table = $('#categoryList').DataTable({
            "processing": true,
            "serverSide": false,
            "ajax":{
                url  : getSubCategory, // json datasource
                type : "post",
                "dataSrc": "values"
            },
            "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                $("td:first", nRow).html(iDisplayIndex +1);
                return nRow;
            },
            "columnDefs": [
                {
                    "targets":0,
                    "render": function(data, type, iDisplayIndex){
                        return iDisplayIndex +1;
                    }
                },
                {
                    "targets": 1,
                    "render": function ( data, type, row ) {
                        var img = row.cat_featured_image ? "<img src='"+row.cat_featured_image+"' width='120px' height='50px'/>" : '';
                        return img;
                    }
                },
                {
                    "targets":2,
                    "data": "name"
                },
                {
                    "targets": 3,
                    "render": function ( data, type, row ) {
                        var parentName = '';
                        for()
                        return img;
                    }
                },
                {
                    "targets":4,
                    "data": "description"
                },
                {
                    "targets": 5,
                    "render": function ( data, type, row ) {
                        return "<a href='"+editUrl+row.categoryId+"' class='btn btn-primary'> <i class='fa fa-edit'></i> Edit </a><a onclick='return confirm(\"Are you Sure to Delete?\")' href='"+removeUrl+row.categoryId+"' class='btn btn-danger'> <i class='fa fa-trash'></i> Remove </a>";
                    }
                }
            ]

        });


        $('#setImage').on('click', '.removeImg', function() {
            $('#setImage').text('');
            $('#image').val('');
            images = '';
        });
    });



</script>
