<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/5/2019
 * Time: 6:47 PM
 */
?>
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="float-left">
                    <h1 class="title">Add User</h1>
                </div>

                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>e
                            <a href="index.html"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="eco-users.html">Users</a>
                        </li>
                        <li class="active">
                            <strong>Add User</strong>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-xl-12 col-lg-12 col-12 col-md-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">User Info</h2>
                    <div class="actions panel_actions float-right">
                        <i class="box_toggle fa fa-chevron-down"></i>
                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                        <i class="box_close fa fa-times"></i>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <form action ="<?= base_url('admin/create_user') ?>" method="post" enctype="multipart/form-data">
                            <div class="col-xl-8 col-lg-8 col-md-9 col-12">

                                <div class="form-group">

                                    <?php
                                    $message = $this->session->userdata('message');
                                    if( isset($message) ){
                                        $type = $message['type']
                                        ?>
                                        <div class="alert <?php echo $type == 'danger' ? 'alert-danger' : 'alert-success'; ?> ">
                                            <button class="close" data-close="alert"></button>
                                            <span><?=$message['text'];?></span>
                                        </div>
                                        <?php
                                    }
                                    ?>


                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="field-1">Full Name</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" required name="full_name" value="" class="form-control" id="field-1" placeholder="Full Name">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-label" for="field-2">User Name</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="text" required name="username" value="" class="form-control" id="field-2" placeholder="User Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="email">Email</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="email" required value="" name="email" class="form-control" id="email" placeholder="example@mail.com">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="role">Role</label>
                                    <span class="desc"></span>
                                    <select required class="form-control" name="role">
                                        <option value="1">Admin</option>
                                        <option value="2">Sub Admin</option>
                                        <option value="3">Users</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="phone">Phone</label>
                                    <div class="controls">
                                        <input type="text" name="phone" value="" class="form-control" id="phone" placeholder="Contact Number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="image">Image</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <input type="file" class="form-control" name="image" id="image">
                                    </div>
                                </div
                                <div class="form-group">
                                    <label class="form-label" for="field-6">Address</label>
                                    <span class="desc"></span>
                                    <div class="controls">
                                        <textarea class="form-control autogrow" name="address" cols="5" id="field-6"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group margin-top-10 padding-bottom-30">
                                <div class="text-right">
                                    <input type="submit" class="btn btn-primary" value="Save" />
                                    <!--                                    <button type="reset" class="btn btn-warning ">Reset</button>-->
                                    <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)">
                                </div>
                            </div>
                    </form>
                </div>
        </div>
    </section>
        </div>
</section>
</section>

