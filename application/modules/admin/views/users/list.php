<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/5/2019
 * Time: 6:47 PM
 */
?>
<!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
            <div class="page-title">

                <div class="float-left">
                    <h1 class="title">Users</h1>
                </div>

                <div class="float-right d-none">
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html"><i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
                            <a href="eco-users.html">Users</a>
                        </li>
                        <li class="active">
                            <strong>All Users</strong>
                        </li>
                    </ol>
                </div>

            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-lg-12">
            <section class="box ">
                <header class="panel_header">
                    <h2 class="title float-left">All Users</h2>
                    <div class="actions panel_actions float-right">
                        <i class="box_toggle fa fa-chevron-down"></i>
                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                        <i class="box_close fa fa-times"></i>
                    </div>
                </header>
                <div class="content-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <?php
                            $message = $this->session->userdata('message');
                            if( isset($message) ){
                                $type = $message['type']
                                ?>
                                <div class="alert <?php echo $type == 'danger' ? 'alert-danger' : 'alert-success'; ?> ">
                                    <button class="close" data-close="alert"></button>
                                    <span><?=$message['text'];?></span>
                                </div>
                                <?php
                            }
                            ?>
                            <!-- ********************************************** -->


                            <table id="list" class="display table table-hover table-condensed" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>#SL</th>
                                    <th>Full Name</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th class="text-center">Manage</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php 
                                //print_r($users);
                                //exit;
                                foreach($users as $key => $user){
                                ?>
                               
                                <tr>
                                    <td><?= $key+1; ?></td>
                                    <td><?= $user['full_name']; ?></td>
                                    <td><?= $user['username']; ?></td>
                                    <td><?= $user['email']; ?></td>
                                    <td>
                                    <?php
                                        if($user['role'] == 1)
                                        {
                                            echo 'Admin';
                                        }else if ($user['role'] == 2)
                                        {
                                            echo 'Sub Admin';
                                        }else
                                        {
                                            echo 'User';
                                        }
                                    ?>
                                    </td>
                                    <td><?= $user['phone']; ?></td>
                                    <td><?= $user['address']; ?></td>
                                    <td class="text-center">
                                        <a href="<?= base_url(); ?>admin/edit_user/<?= $user['id']; ?>" class="btn btn-primary"> <i class="fa fa-edit"> </i> Edit</a>
                                    </td>

                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <!-- ********************************************** -->




                        </div>
                    </div>
                    <div class="row text-right">
                        <input type="button" class="btn btn-warning " value="Back" onClick="javascript:history.go(-1)">  
                    </div>
                </div>
            </section></div>
    </section>
</section>
<!-- END CONTENT -->

