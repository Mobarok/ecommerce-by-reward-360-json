<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/27/2018
 * Time: 6:33 PM
 */


class Login extends MY_Controller{

    public $today;

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Login_Model');

        $this->today = date('Y-m-d H:i:s');
    }


    function check_logged_in()
    {
        $user_id = $this->session->userdata('user_id');
        $username = $this->session->userdata('username');
        $role = $this->session->userdata('role');


        if( !empty($username)|| !empty($user_id)|| !empty($role))
        {
            redirect('/admin/dashboard');
        }

    }

    public function index()
    {
        $this->check_logged_in();

        $data['content'] = 'login/index';
        $data['load_script'] = 'login/script';

        $this->load->view('admin/index', $data);
    }

    public function login_check()
    {
        $username = $this->input->post('username');
        $password = $this->encrypt_decrypt($this->input->post('password'));

        if( empty($username) || empty($password) )
        {
            $message = array(
                'type' => 'danger',
                'text' => "Username Or Password can't empty!!"
            );

            $this->session->set_userdata('message', $message);
            redirect('/login');
        }else{

            $result = $this->Login_Model->login_check($username, $password);

            if( isset($result))
            {
                $this->session->set_userdata('user_id', $result->id);
                $this->session->set_userdata('username', $result->username);
                $this->session->set_userdata('email', $result->email);
                $this->session->set_userdata('role', $result->role);
                $this->session->set_userdata('profileImage', $result->image);

                // 1 = Admin, 2 = Management, 3 = User
                $data['last_login_at'] = $this->today;

                $this->Login_Model->update_function('id',$result->id, 'users', $data);

                if($result->role == 1 || $result->role == 2 || $result->role == 3)
                {
                    $this->session->set_userdata('APIKey', "SADJKLASKDJLKJKLASJDKLJKJSA");
                    $this->session->set_userdata('VendorID', "ABC123");
                }

                if($result->role == 1)
                    redirect('/admin/dashboard');
                elseif($result->role == 2)
                    redirect('/admin/dashboard');
                elseif($result->role == 3)
                    redirect('/admin/dashboard');
                else{
                    $message = array(
                        'type' => 'danger',
                        'text' => "Invalid username Or Password !!"
                    );

                    $this->session->set_userdata('message', $message);
                    redirect('login','refresh');
                }


            }else{

                $message = array(
                    'type' => 'danger',
                    'text' => "Invalid username Or Password !!"
                );

                $this->session->set_userdata('message', $message);
                redirect('/login');
            }
        }
    }


    public function logout( $id='' )
    {

        $username = $this->session->userdata('username');
        $result = $this->Login_Model->logout($id, $username);

        if(isset($result)){
            $this->session->unset_userdata('user_id');
            $this->session->unset_userdata('username');
            $this->session->unset_userdata('role');

            $message = array(
                'type' => 'success',
                'text' => "You have successfully logged out from the system"
            );

            $this->session->set_userdata('message', $message);
            redirect('/login');

        }

        redirect('/dashboard');
    }
}