<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/27/2018
 * Time: 5:17 PM
 */

class Login_Model extends CI_Model{

    /* --------- Status ---------
         *  For status check
         *  Status 1 for approved
         *  Status 0 for Unapproved
        */

    public function __construct()
    {
        parent::__construct();
    }


    public function login_check($username, $password)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->where('status', 1);
        $query = $this->db->get();

        return $query->row();
    }


    public function logout( $id, $username )
    {
        $this->db->select('id, username, role');
        $this->db->from('users');
        $this->db->where('id', $id);
        $this->db->where('username', $username);
        $query = $this->db->get();

        return $query->row();
    }

    public function update_function($columnName, $columnVal, $tableName, $data)
    {

        $this->db->where($columnName, $columnVal);
        return $this->db->update($tableName, $data);
    }


}