<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/27/2018
 * Time: 6:31 PM
 */
?>

<div class="login-wrapper">
    <div id="login" class="login loginpage offset-xl-4 col-xl-4 offset-lg-3 col-lg-6 offset-md-3 col-md-6 col-offset-0 col-12">
        <h1><a href="#" title="Login Page" tabindex="-1" style="background-image: url('<?php echo base_url();?>admin-assets/assets/images/JCMS-logo.png');">JSMSproRewards</a></h1>

        <form name="loginform" id="loginform" action="<?php echo base_url()?>login/login_check" method="post">
            <?php
            $message = $this->session->userdata('message');
            if( isset($message) ){
                $type = $message['type']
                ?>
                <div class="alert <?php echo $type == 'danger' ? 'alert-danger' : 'alert-success'; ?> ">
                    <button class="close" data-close="alert"></button>
                    <span><?=$message['text'];?></span>
                </div>
                <?php
                $this->session->unset_userdata('message');
            }
            ?>
            <div class="form-group">
                <label for="user_login">Username<br />
                    <input type="text" name="username" id="username" value="" class="input" size="20" />
                </label>
            </div>
            <div class="form-group">
                <label for="user_pass">Password<br />
                    <input type="password" name="password" id="password" value="" class="input" size="20" />
                </label>
            </div>
            <div class="forgetmenot">
                <label class="icheck-label form-label" for="rememberme">
                    <input name="rememberme" type="checkbox" id="rememberme" value="forever" class="skin-square-orange"> Remember me
                </label>
            </div>
            <div class="submit">
                <input type="submit" name="submit" id="submit" class="btn btn-info btn-block" value="Sign In" />
            </div>
        </form>

<!--        <div id="nav">-->
<!--            <a class="float-left info" href="#" title="Password Lost and Found">Forgot password?</a>-->
<!--            <a class="float-right info" href="--><?php //echo base_url();?><!--register" title="Sign Up">Sign Up</a>-->
<!--        </div>-->


    </div>
</div>
