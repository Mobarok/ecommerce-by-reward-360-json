<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/25/2018
 * Time: 1:52 PM
 */

?>


<script type="text/javascript">

    $(document).ready(function() {
        $('#submit').click( function ( ) {

            var username = $('#username').val();
            var password = $('#password').val();

            if( username == '' )
            {
                $('#username').css('border', '1px solid #ff0534d6');
            }

            if( password == '' )
            {
                $('#password').css('border', '1px solid #ff0534d6');
            }

            if( username == '' || password == '' )
            {
                return false;
            }

            $("#login").submit();

        });
    });
</script>
