<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/2/2019
 * Time: 10:40 AM
 */

class Register extends MY_Controller {


    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Register_Model');
    }

    public function index()
    {
        $data['content'] = 'register/index';
        $data['load_script'] = 'register/script';

        $this->load->view('admin/index', $data);
    }

    public function register_form()
    {

        $password =  $this->input->post('password');
        $re_password =  $this->input->post('passwordConfirm');

        if( $password != $re_password )
        {
            $message = array(
                'type' => 'danger',
                'text' => 'Password do not match!',
            );
            $this->session->set_userdata('message', $message);

            redirect('register');
        }


        $valid_email = $this->email_validation($this->input->post('email'));
        $check_unique_email = $this->check_email_unique();
        $check_unique_username = $this->check_username_unique();

        if( $check_unique_email==1 || $check_unique_username==1 ) {
            redirect('register');
        }

        $data = [];
        $data['username'] = $this->input->post('username');
        $data['email'] = $this->input->post('email');
        $data['password'] =  $this->encrypt_decrypt($this->input->post('password'));

        $this->db->trans_start();

        $result = $this->Register_Model->register($data);

        $user_info['full_name'] = $this->input->post('full_name');
        $user_info['user_id'] = $result;

        $user_info = $this->Register_Model->user_info($user_info);

        $this->db->trans_complete();

        if($user_info>0 && $result>0)
        {
            $message = array(
                'type' => 'success',
                'text' => 'Please wait for Admin Confirmation',
            );
            $this->session->set_userdata('message', $message);

        }else{

            $message = array(
                'type' => 'danger',
                'text' => 'please check again !!',
            );
            $this->session->set_userdata('message', $message);

        }

        redirect('register');
    }

    public function check_email_unique()
    {
        $email= $this->input->post('email');
        $data= $this->Register_Model->check_data('email', $email, 'users');

        if($data>0)
            return 1;
        else
            return 0;

    }


    public function check_username_unique()
    {
        $username= $this->input->post('username');
        $data= $this->Register_Model->check_data('username', $username, 'users');
        if($data>0)
        {
            return 1;
        }else{
            return 0;
        }

    }



}