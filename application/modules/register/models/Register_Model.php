<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/2/2019
 * Time: 10:41 AM
 */


class Register_Model extends CI_Model{

    public function register($user)
    {
        // Status: Inactive = 2
        // Role: User = 3

        $user['status'] = 2;
        $user['role'] = 3;

        $this->db->insert('users', $user);

        $insert_id = $this->db->insert_id();
        return  $insert_id;

    }


    public function user_info($user_info)
    {
        $this->db->insert('user_info', $user_info);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }


    public function check_data($column_name, $data)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($column_name, $data);
        $result = $this->db->get();

        return $result->num_rows();
    }


}
