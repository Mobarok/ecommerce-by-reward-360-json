<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/2/2019
 * Time: 10:41 AM
 */

?>


<div class="register-wrapper">
    <div id="register" class="login loginpage offset-xl-4 col-xl-4 offset-lg-3 col-lg-6 offset-md-3 col-md-6 col-offset-0 col-12">
        <h1>
            <a href="<?=base_url()?>" title="Login Page" tabindex="-1" style="background-image: url('<?php echo base_url();?>admin-assets/assets/images/JCMS-logo.png');">JSMSproRewards</a></h1>


        <form name="registerForm" id="registerForm" action="<?=base_url()?>/register/register_form" method="post">
            <?php
            $message = $this->session->userdata('message');
            if( isset($message) ){
                $type = $message['type']
                ?>
                <div class="alert <?php echo $type == 'danger' ? 'alert-danger' : 'alert-success'; ?> ">
                    <button class="close" data-close="alert"></button>
                    <span><?=$message['text'];?></span>
                </div>
                <?php
                $this->session->unset_userdata('message');
            }
            ?>

            <p>
                <label for="user_login">Full Name<br />
                    <input type="text" name="full_name" id="full_name" class="input" value="" size="20" />
                </label>
            </p>
            <p>
                <label for="user_login">Email<br />
                    <input type="email" name="email" id="email" class="input" value="" size="20" /></label>
            </p>
            <p>
                <label for="user_login">Username<br />
                    <input type="text" name="username" id="username" class="input" value="" size="20" /></label>
            </p>
            <p>
                <label for="user_pass">Password<br />
                    <input type="password" name="password" id="password" class="input" value="" size="20" /></label>
            </p>
            <p>
                <label for="user_pass">Confirm Password<br />
                    <input type="password" name="passwordConfirm" id="passwordConfirm" class="input" value="" size="20" />
                </label>
            </p>
            <p class="forgetmenot">
                <label class="icheck-label form-label" for="rememberme">
                    <input name="rememberme" type="checkbox" id="rememberme" value="forever" class="skin-square-orange" checked> I agree to terms to conditions</label>
            </p>



            <p class="submit">
                <input type="submit" name="wp-submit" id="wp-submit" class="btn btn-orange btn-block" value="Sign Up" />
            </p>
        </form>

        <p id="nav">
            <a class="float-left" href="#" title="Password Lost and Found">Forgot password?</a>
            <a class="float-right" href="<?php base_url();?>login" title="Sign Up">Sign In</a>
        </p>
        <div class="clearfix"></div>
        <div class="col-lg-12 text-center register-social">

            <a href="#" class="btn btn-primary btn-lg facebook">
                <i class="fa fa-facebook icon-sm"></i>
            </a>
            <a href="#" class="btn btn-primary btn-lg twitter">
                <i class="fa fa-twitter icon-sm"></i>
            </a>
            <a href="#" class="btn btn-primary btn-lg google-plus">
                <i class="fa fa-google-plus icon-sm"></i>
            </a>
            <a href="#" class="btn btn-primary btn-lg dribbble">
                <i class="fa fa-dribbble icon-sm"></i>
            </a>

        </div>

    </div>
</div>
