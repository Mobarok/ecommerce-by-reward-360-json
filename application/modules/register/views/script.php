<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 1/2/2019
 * Time: 12:52 PM
 */
?>


<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

<script type="text/javascript">

    $(document).ready(function() {

        $('form[id="registerForm"]').validate({
            rules: {
                username: 'required',
                full_name: 'required',
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true,
                    minlength: 5,
                },
                passwordConfirm: {
                    minlength : 5,
                    equalTo : "#password"
                }
            },
//                errorPlacement: function(){
//                return false;
//            },
            messages: {
                full_name: 'This field is required',
                username: 'This field is required',
                email: 'Enter a valid email',
                password: {
                    minlength: 'Password must be at least 5 characters long'
                }
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
    });
</script>
