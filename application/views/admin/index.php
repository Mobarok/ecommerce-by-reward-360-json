<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/27/2018
 * Time: 5:18 PM
 */


$login_id = $this->session->userdata('user_id');
$role = $this->session->userdata('role');
?>

<!DOCTYPE html>
<html class=" ">
<!--<base href="--><?//=base_url();?><!--">-->
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title><?php if(!empty($this->config->item('siteName'))) echo $this->config->item('siteName'); else echo "JCMS Pro Rewards"; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />


    <!-------------- ****** Load Styles ****** ------------->
    <?php $this->load->view('admin/styles'); ?>


</head>

<!-- BEGIN BODY -->
<body class="login_page">


    <!-------------- ******  Load Top Navigation for Admin Users ****** ---------->
    <?php
         if (isset($login_id) && isset($role))
             $this->load->view('admin/top_nav');
    ?>

    <div class="page-container row-fluid">


        <!--------- ******  Load Left Sidebar for Admin Users ****** ------------>
        <?php
            if(isset($login_id) && isset($role))
                $this->load->view('admin/left_sidebar');
        ?>


        <!-------------- ******  Load Body Content ****** ----------->
        <?php $this->load->view($content); ?>


        <!--------- ******  Load Right Sidebar for Admin Users ****** ------------>
        <?php
        if(isset($login_id) && isset($role)) {
            $this->load->view('admin/right_sidebar');

            ?>
            <div class="footer-text">
<!--                <div class="footer-v text-center">-->
<!--                    <p>Version: JCMS v01-2019</p>-->
<!--                </div>-->
                <div class="footer-version">
                    <p class="float-right">JCMS ProRewards Sdn Bhd. </p>
                </div>
            </div>

            <?php
        }
        ?>

    </div>



    <!-------------- ******  Load JavaScripts Codes ****** --------------->

    <?php $this->load->view('admin/js_scripts'); ?>


    <!-- General section box modal start -->
    <div class="modal" id="section-settings" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
        <div class="modal-dialog animated bounceInDown">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Section Settings</h4>
                </div>
                <div class="modal-body">

                    Body goes here...

                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                    <button class="btn btn-success" type="button">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- modal end -->
</body>
</html>

