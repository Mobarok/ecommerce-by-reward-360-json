<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/27/2018
 * Time: 5:03 PM
 */

?>


<!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->



<!-- CORE JS FRAMEWORK - START -->
<script src="<?php echo base_url();?>admin-assets/assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>admin-assets/assets/js/popper.min.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url();?>assets/js/jquery.easing.min.js" type="text/javascript"></script>  -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>


<script src="<?php echo base_url();?>admin-assets/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>admin-assets/assets/plugins/pace/pace.min.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>admin-assets/assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>admin-assets/assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>
<!-- CORE JS FRAMEWORK - END -->


<!--<script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript"></script>-->
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
<script src="<?php echo base_url();?>admin-assets/assets/plugins/icheck/icheck.min.js" type="text/javascript"></script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>

<!--<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>-->

<!-------------- ****** For New External JavaScripts Load ****** ------------->

<?php isset($load_script) ? $this->load->view($load_script) : ''; ?>

<!-- CORE TEMPLATE JS - START -->
<script src="<?php echo base_url();?>admin-assets/assets/js/scripts.js" type="text/javascript"></script>
<!-- END CORE TEMPLATE JS - END -->

<!-- Sidebar Graph - START -->
<script src="<?php echo base_url();?>admin-assets/assets/plugins/sparkline-chart/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>admin-assets/assets/js/chart-sparkline.js" type="text/javascript"></script>
<!-- Sidebar Graph - END -->

<script type="application/javascript">


    var windowWidth = $( window ).width();
    var setFooterWidth = (windowWidth-300-50)


    var ULTRA_SETTINGS = window.ULTRA_SETTINGS || {};

    $(document).ready(function() {

        $('#list').DataTable();
        $('.selectpicker').selectpicker();
//        $( '.footer-version' ).width(setFooterWidth);

        var sidebar = localStorage.getItem("sidebar");

//        var chatarea = $(".page-chatapi");
//        var chatwindow = $(".chatapi-windows");
//        var topbar = $(".page-topbar");
//        var mainarea = $("#main-content");
//        var menuarea = $(".page-sidebar");

        console.log(sidebar);
        if(sidebar=="sidebar_shift")
        {
            $(".page-sidebar").addClass("collapseit");
//                .removeClass("expandit");
            $(".page-topbar").addClass("sidebar_shift");
            $("#main-content").addClass("sidebar_shift");
            $("#page-sidebar-wrapper").removeClass("ps");
            $("#page-sidebar-wrapper").removeClass("ps--active-y");

            ULTRA_SETTINGS.mainmenuCollapsed();
        }else{
            $(".page-sidebar").addClass("expandit");
//                .removeClass("collapseit");
            $(".page-topbar").removeClass("sidebar_shift");
            $("#main-content").removeClass("sidebar_shift");
            localStorage.removeItem("sidebar");
        }
    });



</script>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

<!--<script type="application/javascript">-->
<!--    function confirmDelete(sender) {-->
<!--        bootbox.confirm("Are you sure you want to delete?", function (confirmed) {-->
<!--            if (confirmed) {-->
<!--                return true;-->
<!--            }else{-->
<!--                return false;-->
<!--            }-->
<!--        });-->
<!--    }-->
<!--</script>-->