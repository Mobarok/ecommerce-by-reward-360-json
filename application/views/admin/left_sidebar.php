<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/27/2018
 * Time: 5:53 PM
 */

$role = $this->session->userdata('role');
$username = $this->session->userdata('username');
$image = $this->session->userdata('profileImage');
?>

<!-- SIDEBAR - START -->
<div class="page-sidebar ">

    <!-- MAIN MENU - START -->
    <div class="page-sidebar-wrapper" id="main-menu-wrapper">

        <!-- USER INFO - START -->
        <div class="profile-info row">

            <div class="profile-image col-lg-4 col-md-4 col-4">
                <a href="#">
                    <?php
                    if( !empty($image)) {
                        ?>
                        <img src="<?=$image?>" class="img-fluid rounded-circle">
                        <?php
                    }else{ ?>
                        <img src="<?= base_url() ?>admin-assets/data/profile/user.png" class="img-fluid rounded-circle">
                        <?php
                    }
                    ?>
                </a>
            </div>

            <div class="profile-details col-lg-8 col-md-8 col-8">

                <h3>
                    <a href="#"><?=$username?></a>

                    <!-- Available statuses: online, idle, busy, away and offline -->
                    <span class="profile-status online"></span>
                </h3>

                <p class="profile-title">Admin</p>

            </div>

        </div>
        <!-- USER INFO - END -->



        <ul class='wraplist'>


            <li class="">
                <a href="<?=base_url().'admin/dashboard';?>">
                    <i class="fa fa-dashboard"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>

            <li class="">
                <a href="javascript:;">
                    <i class="fa fa-sitemap"></i>
                    <span class="title">Manage Categories</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >
                    <li>
                        <a class="" href="<?=base_url()?>admin/add_category" >Add Category</a>
                    </li>
                    <li>
                        <a class="" href="<?=base_url()?>admin/categories" >Category List</a>
                    </li>
                </ul>
            </li>

            <li class="">
                <a href="javascript:;">
                    <i class="fa fa-sitemap"></i>
                    <span class="title">Manage Sub-Categories</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >
                    <li>
                        <a class="" href="<?=base_url()?>admin/add_subcategory" >Add Sub-Category</a>
                    </li>
                    <li>
                        <a class="" href="<?=base_url()?>admin/subcategories" >Sub-Category List</a>
                    </li>
                </ul>
            </li>
<!--            <li class="">-->
<!--                <a href="javascript:;">-->
<!--                    <i class="fa fa-building"></i>-->
<!--                    <span class="title">Manage Brands</span>-->
<!--                    <span class="arrow "></span>-->
<!--                </a>-->
<!--                <ul class="sub-menu" >-->
<!--                    <li>-->
<!--                        <a class="" href="--><?//=base_url()?><!--admin/add_brand" >Add Brands</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a class="" href="--><?//=base_url()?><!--admin/brands" >Brand List</a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </li>-->
            <li class="">
                <a href="javascript:;">
                    <i class="fa fa-cubes"></i>
                    <span class="title">Manage Products</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >

                    <li>
                        <a class="" href="<?=base_url()?>admin/add_product" >Add Product</a>
                    </li>
                    <li>
                        <a class="" href="<?=base_url()?>admin/products" >Products List</a>
                    </li>
                </ul>
            </li>

<!--            <li class="">-->
<!--                <a href="javascript:;">-->
<!--                    <i class="fa fa-bookmark"></i>-->
<!--                    <span class="title">Manage Product Type</span>-->
<!--                    <span class="arrow "></span>-->
<!--                </a>-->
<!--                <ul class="sub-menu" >-->
<!--                    <li>-->
<!--                        <a class="" href="" >All Product Type</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a class="" href="" >Add Product Type</a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </li>-->
<!--            <li class="">-->
<!--                <a href="javascript:;">-->
<!--                    <i class="fa fa-bookmark"></i>-->
<!--                    <span class="title">Manage Redemption Type</span>-->
<!--                    <span class="arrow "></span>-->
<!--                </a>-->
<!--                <ul class="sub-menu" >-->
<!--                    <li>-->
<!--                        <a class="" href="" >All Redemption Type</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a class="" href="" >Add Redemption Type</a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </li>-->

            <li class="">
                <a href="javascript:;">
                    <i class="fa fa-list"></i>
                    <span class="title">Manage Stock</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >
                    <li>
                        <a class="" href="<?=base_url()?>admin/stocks" >Stock List</a>
                    </li>
                </ul>
            </li>

            <li class="">
                <a href="javascript:;">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="title">Manage Orders</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >
<!--                    <li>-->
<!--                        <a class="" href="--><?//=base_url()?><!--admin/orders" >Place of Orders</a>-->
<!--                    </li>-->
                    <li>
                        <a class="" href="<?=base_url()?>admin/order_history" >Order History</a>
                    </li>
                </ul>
            </li>

            <li class="">
                <a href="javascript:;">
                    <i class="fa fa-money"></i>
                    <span class="title">Manage Purchases</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >
                    <li>
                        <a class="" href="<?=base_url()?>admin/add_purchase" >Add Purchase</a>
                    </li>
                    <li>
                        <a class="" href="<?=base_url()?>admin/purchases" >Purchase List</a>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="javascript:;">
                    <i class="fa fa-truck"></i>
                    <span class="title">Manage Shipment</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu" >
                    <li>
                        <a class="" href="<?=base_url()?>admin/shipment_schedule" >Shipment Schedule</a>
                    </li>
                    <li>
                        <a class="" href="<?=base_url()?>admin/shipment_history" >Shipment History</a>
                    </li>
                </ul>
            </li>
            <?php

            if(isset($role))
            {
                if($role != 3) {
                    ?>
                    <li class="">
                        <a href="javascript:;">
                            <i class="fa fa-user"></i>
                            <span class="title">Users</span>
                            <span class="arrow "></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a class="" href="<?php echo base_url() . 'admin/users'; ?>">All Users</a>
                            </li>
                            <li>
                                <a class="" href="<?php echo base_url() . 'admin/add_user'; ?>">Add User</a>
                            </li>
                            <li>
                                <a class="" href="<?php echo base_url() . 'admin/user_status/active'; ?>">Active
                                    User</a>
                            </li>

                            <li>
                                <a class="" href="<?php echo base_url() . 'admin/user_status/inactive'; ?>">Inactive
                                    User</a>
                            </li>
                        </ul>
                    </li>
                    <li class="">
                        <a href="javascript:;">
                            <i class="fa fa-gear"></i>
                            <span class="title">Settings</span>
                            <span class="arrow "></span>
                        </a>
                        <ul class="sub-menu" style="margin-bottom: 20px; clear: both;">
                            <li>
                                <a class="" href="<?php echo base_url() . 'admin/settings'; ?>">General Settings</a>
                            </li>
                            <li>
                                <a class="" href="<?php echo base_url() . 'admin/profile'; ?>">Profile</a>
                            </li>
                            <li>
                                <a class="" href="<?php echo base_url() . 'admin/change_password'; ?>">Change Password</a>
                            </li>
                        </ul>
                    </li>

                    <?php
                }
            }
            ?>
            
<!--            <li class="open">-->
<!--                <a href="javascript:;">-->
<!--                    <i class="fa fa-users"></i>-->
<!--                    <span class="title">Customers</span>-->
<!--                    <span class="arrow open"></span>-->
<!--                </a>-->
<!--                <ul class="sub-menu" style='display:block;'>-->
<!--                    <li>-->
<!--                        <a class="" href="eco-customer-add.html" >Add Customer</a>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <a class="" href="eco-customers.html" >All Customers</a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </li>-->

        </ul>

    </div>
    <!-- MAIN MENU - END -->



    <div class="project-info text-center">

        <p class="footer-v">Version: JCMS v01-2019</p>
<!--        <div class="block1">-->
<!--            <div class="data">-->
<!--                <span class='title'>Orders</span>-->
<!--                <span class='total'>545</span>-->
<!--            </div>-->
<!--            <div class="graph">-->
<!--                <span class="sidebar_orders">...</span>-->
<!--            </div>-->
<!--        </div>-->

<!--        <div class="block2">-->
<!--            <div class="data">-->
<!--                <span class='title'>Customers</span>-->
<!--                <span class='total'>3146</span>-->
<!--            </div>-->
<!--            <div class="graph">-->
<!--                <span class="sidebar_visitors">...</span>-->
<!--            </div>-->
<!--            </div>-->
<!--        </div>-->

    </div>



</div>
<!--  SIDEBAR - END -->

