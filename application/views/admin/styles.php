<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/27/2018
 * Time: 5:04 PM
 */
?>

<?php
 if(!empty($this->config->item('favicon')))
 {
    echo '<link rel="shortcut icon" href="'.$this->config->item('favicon').'" type="image/x-icon" />';
 }else{
     echo '<link rel="shortcut icon" href="'.base_url().'"admin-assets/assets/images/favicon-jcms.png" type="image/x-icon" />';
 }
?>
  <!-- Favicon -->
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>admin-assets/assets/images/apple-touch-icon-57-precomposed.png">	<!-- For iPhone -->
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>admin-assets/assets/images/apple-touch-icon-114-precomposed.png">    <!-- For iPhone 4 Retina display -->
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>admin-assets/assets/images/apple-touch-icon-72-precomposed.png">    <!-- For iPad -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>admin-assets/assets/images/apple-touch-icon-144-precomposed.png">    <!-- For iPad Retina display -->




<!-- CORE CSS FRAMEWORK - START -->
<link href="<?php echo base_url();?>admin-assets/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?php echo base_url();?>admin-assets/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>admin-assets/assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>admin-assets/assets/css/animate.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>admin-assets/assets/plugins/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" type="text/css"/>
<!-- CORE CSS FRAMEWORK - END -->

<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
<link href="<?php echo base_url();?>admin-assets/assets/plugins/icheck/skins/square/orange.css" rel="stylesheet" type="text/css" media="screen"/>        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


<!-- CORE CSS TEMPLATE - START -->
<link href="<?php echo base_url();?>admin-assets/assets/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>admin-assets/assets/css/responsive.css" rel="stylesheet" type="text/css"/>
<!-- CORE CSS TEMPLATE - END -->

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>


<!-------------- ****** For New External Styles Load ****** ------------->
<?php isset($load_style) ? $this->load->view($load_style) : ''; ?>
