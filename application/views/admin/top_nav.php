<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 12/27/2018
 * Time: 5:53 PM
 */


$user_id = $this->session->userdata('user_id');
$username = $this->session->userdata('username');
$image = $this->session->userdata('profileImage');

if(!empty($this->config->item('logo'))) {
?>
<style type="text/css">
    .page-topbar .logo-area {
        background-image: url("<?=$this->config->item('logo');?>");
    }

    .page-topbar.sidebar_shift .logo-area {
        background-image: url("<?=$this->config->item('logo');?>");
    }
</style>
<?php
}
?>

<!-- START TOPBAR -->
<div class='page-topbar '>
    <div class='logo-area'>

    </div>
    <div class='quick-area'>
        <div class='float-left'>
            <ul class="info-menu left-links list-inline list-unstyled">
                <li class="sidebar-toggle-wrap list-inline-item">
                    <a href="#" data-toggle="sidebar" class="sidebar_toggle">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
                <li class="notify-toggle-wrapper list-inline-item">
                    <a href="#" data-toggle="dropdown" class="toggle">
                        <i class="fa fa-bell"></i>
                        <span class="badge badge-pill badge-orange">0</span>
                    </a>
                    <ul class="dropdown-menu notifications animated fadeIn">
                        <li class="total dropdown-item">
                                    <span class="small">
                                        You have <strong>0</strong> new notifications.
                                        <a href="javascript:;" class="float-right">Mark all as Read</a>
                                    </span>
                        </li>
                        <li class="list dropdown-item">

                            <ul class="dropdown-menu-list list-unstyled ps-scrollbar">


                            </ul>

                        </li>

                        <li class="external dropdown-item">
                            <a href="javascript:;">
                                <span>Read All Notifications</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="d-sm-none d-none searchform list-inline-item">
                    <div class="input-group">
                                <span class="input-group-addon input-focus">
                                    <i class="fa fa-search"></i>
                                </span>
                        <form action="search-page.html" method="post">
                            <input type="text" class="form-control animated fadeIn" placeholder="Search & Enter">
                            <input type='submit' value="">
                        </form>
                    </div>
                </li>
            </ul>
        </div>
        <div class='float-right'>
            <ul class="info-menu right-links list-inline list-unstyled">
                <li class="profile list-inline-item">
                    <a href="#" data-toggle="dropdown" class="toggle">
                        <?php
                        if( !empty($image)) {
                            ?>
                            <img src="<?=$image?>" class="rounded-circle img-inline">
                            <?php
                        }else{ ?>
                            <img src="<?= base_url() ?>admin-assets/data/profile/user.png" class="rounded-circle img-inline">
                            <?php
                        }
                        ?>
                        <span class="text-capital" ><?=$username?> <i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul class="dropdown-menu profile animated fadeIn">
                        <li class="dropdown-item">
                            <a href="<?=base_url().'admin/change_password';?>">
                                <i class="fa fa-wrench"></i>
                                Change Password
                            </a>
                        </li>
                        <li class="dropdown-item">
                            <a href="<?=base_url().'admin/profile';?>">
                                <i class="fa fa-user"></i>
                                Profile
                            </a>
                        </li>
                        <li class="last dropdown-item" >
                            <?php $url =  base_url().'login/logout/'.$user_id; ?>
                            <a href="<?php echo $url;?>" onclick="return confirm('Are you sure you wish to logout?');">
                                <i class="fa fa-lock"></i>
                                Logout
                            </a>
                        </li>
                    </ul>
                </li>
                <!--<li class="chat-toggle-wrapper list-inline-item">-->
                <!--    <a href="<?=base_url()?>admin/dashboard/#" data-toggle="chatbar" class="toggle_chat">-->
                <!--        <i class="fa fa-comments"></i>-->
                <!--        <span class="badge badge-pill badge-warning">9</span>-->
                <!--        <i class="fa fa-times"></i>-->
                <!--    </a>-->
                <!--</li>-->
            </ul>
        </div>
    </div>

</div>
<!-- END TOPBAR -->
