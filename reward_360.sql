-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 10, 2019 at 10:48 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `reward_360`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categoryId` int(11) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1=Published,  2=Unpublished',
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `slug`, `image`, `description`, `categoryId`, `status`, `created`, `updated`) VALUES
(1, 'SIMPLEE', '', '', '', 0, 1, NULL, NULL),
(2, 'LOVE&LEMONADE', '', '', '', 0, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `categoryId` int(11) UNSIGNED NOT NULL,
  `countryId` int(11) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parentId` int(11) UNSIGNED NOT NULL,
  `displayOrder` int(11) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Published,  2=Unpublished',
  `id` int(11) UNSIGNED NOT NULL,
  `vendorId` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`categoryId`, `countryId`, `name`, `parentId`, `displayOrder`, `created`, `updated`, `description`, `slug`, `status`, `id`, `vendorId`) VALUES
(1, 0, 'Women’s Clothing', 0, 0, NULL, NULL, '', 'women-clothing', 1, 0, NULL),
(2, 0, 'Men’s Clothing', 0, 0, NULL, NULL, '', 'men-clothing', 1, 0, NULL),
(3, 0, 'Cellphones & Accessories', 0, 0, NULL, NULL, '', '', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `countryId` int(10) UNSIGNED NOT NULL,
  `countryName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `couriers`
--

CREATE TABLE `couriers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referenceNumber` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1=Published,  2=Unpublished',
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `currencyCode` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currencySymbal` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `countryId` int(11) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Published,  2=Unpublished',
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_01` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_02` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postalCode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `countryId` int(11) UNSIGNED NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `userId` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `customerID` int(10) UNSIGNED NOT NULL,
  `discount` int(11) DEFAULT NULL,
  `total_price` double NOT NULL,
  `orderReference` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderDate` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paymentStatus` int(11) DEFAULT NULL,
  `lastModifiedBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastModifiedDate` int(11) DEFAULT NULL,
  `orderRemark` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancelationReason` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancelledQuantity` int(11) DEFAULT NULL,
  `cancellableQuantity` int(11) DEFAULT NULL,
  `poGeneratedDate` date DEFAULT NULL,
  `rtoDate` date DEFAULT NULL,
  `rtoReason` date DEFAULT NULL,
  `expectedDeliveryDate` date DEFAULT NULL,
  `productOrderReference` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productConversionRatio` double DEFAULT NULL,
  `redemptionType` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pointsUsed` int(11) DEFAULT NULL,
  `pointsReversed` int(11) DEFAULT NULL,
  `pointsToReverse` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `amountPaid` int(11) DEFAULT NULL,
  `currencyCode` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendorID` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `cancelledDate` date DEFAULT NULL,
  `outOfStock` tinyint(4) DEFAULT NULL,
  `courierReferenceNumber` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dispatchedDate` date DEFAULT NULL,
  `deliveryDate` date DEFAULT NULL,
  `freeField1_10` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `purchaseId` int(10) UNSIGNED DEFAULT NULL,
  `productId` int(10) UNSIGNED DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `total_price` int(11) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `updated` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `productId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `countryId` int(11) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `specification` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `productCategories` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Product Category ID separated by comma',
  `similarProducts` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Similar Products ID separated by comma',
  `active` tinyint(1) NOT NULL COMMENT '1=Active, 0=Inactive',
  `brand` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Brand String For API',
  `stockCount` int(11) NOT NULL,
  `total_sell` int(11) DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `validFrom` date DEFAULT NULL,
  `validTill` date DEFAULT NULL,
  `customerCategory` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Customer Category ID separated by comma',
  `customerFlags` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Customer Flags ID separated by comma',
  `total_view` int(11) DEFAULT NULL,
  `productType` int(10) UNSIGNED NOT NULL COMMENT 'Product Type by ID',
  `redemptionType` int(10) UNSIGNED NOT NULL COMMENT 'Redemption Type by ID',
  `voucherId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'voucher ID for API',
  `price_mrp` int(11) NOT NULL COMMENT 'Product Price MRP Rate',
  `client_price` int(11) NOT NULL COMMENT 'Product Client Price',
  `discount` int(11) NOT NULL COMMENT 'Product discount %',
  `minimumPoints` int(11) NOT NULL COMMENT 'To buy Minimum Point',
  `minimumCash` int(11) NOT NULL COMMENT 'To buy Minimum cash',
  `ratio` double DEFAULT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Seo Key Words separated by Comma',
  `basePoints` int(11) DEFAULT NULL,
  `actualPoints` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `shortDescription` varchar(245) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `venderProductStatus` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `similarProductsByVendor` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partnerId` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `venderSku` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subCustomerCategory` int(11) DEFAULT NULL,
  `vendorCategory` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `productId`, `countryId`, `name`, `specification`, `description`, `productCategories`, `similarProducts`, `active`, `brand`, `stockCount`, `total_sell`, `sku`, `validFrom`, `validTill`, `customerCategory`, `customerFlags`, `total_view`, `productType`, `redemptionType`, `voucherId`, `price_mrp`, `client_price`, `discount`, `minimumPoints`, `minimumCash`, `ratio`, `keywords`, `basePoints`, `actualPoints`, `created`, `updated`, `shortDescription`, `status`, `venderProductStatus`, `similarProductsByVendor`, `partnerId`, `venderSku`, `subCustomerCategory`, `vendorCategory`) VALUES
(1, '1', 0, 'Hoddies', '', '', '', '', 1, '', 0, NULL, '', NULL, NULL, '', '', NULL, 0, 0, '', 0, 0, 0, 0, 0, NULL, '', NULL, NULL, '0000-00-00 00:00:00', '2019-01-09 13:38:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '2', 0, 'Shirt', '', '', '', '', 1, '', 0, NULL, '', NULL, NULL, '', '', NULL, 0, 0, '', 0, 0, 0, 0, 0, NULL, '', NULL, NULL, '0000-00-00 00:00:00', '2019-01-09 13:39:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `imageId` int(10) UNSIGNED NOT NULL,
  `productId` int(10) UNSIGNED NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `displayOrderId` tinyint(4) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE `product_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `displayName` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Published,  2=Unpublished',
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `purchase_id` int(11) NOT NULL,
  `vendor` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `remark` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `total_price` int(11) NOT NULL,
  `payment` int(11) NOT NULL,
  `due` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '0=paid,1=unpaid,2=cancel',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_details`
--

CREATE TABLE `purchase_details` (
  `purchase_details_id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `product_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `product_price` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `product_category_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `brand` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


--
-- Table structure for table `redempation_types`
--

CREATE TABLE `redempation_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `displayName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shipments`
--

CREATE TABLE `shipments` (
  `id` int(11) NOT NULL,
  `orderID` int(10) UNSIGNED DEFAULT NULL,
  `orderReference` varchar(45) DEFAULT NULL,
  `customerID` int(10) UNSIGNED NOT NULL,
  `productID` varchar(20) NOT NULL,
  `productName` varchar(50) NOT NULL,
  `productOrderReference` varchar(150) DEFAULT NULL,
  `productConversionRatio` double DEFAULT NULL,
  `productPointsValue` int(11) DEFAULT NULL,
  `redemptionType` varchar(45) DEFAULT NULL,
  `pointsUsed` int(11) DEFAULT NULL,
  `pointsReversed` int(11) DEFAULT NULL,
  `pointsToReverse` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `amountPaid` int(11) DEFAULT NULL,
  `currencyCode` varchar(5) DEFAULT NULL,
  `vendorID` varchar(45) DEFAULT NULL,
  `vendorStatus` int(11) DEFAULT NULL,
  `paymentStatus` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `orderDate` varchar(45) DEFAULT NULL,
  `deliveryDate` date DEFAULT NULL,
  `dispatchedDate` date DEFAULT NULL,
  `lastModifiedBy` varchar(100) DEFAULT NULL,
  `lastModifiedDate` int(11) DEFAULT NULL,
  `orderRemark` varchar(45) DEFAULT NULL,
  `courierReferenceNumber` varchar(45) DEFAULT NULL,
  `courierName` varchar(45) DEFAULT NULL,
  `cancelationReason` varchar(250) DEFAULT NULL,
  `cancelledQuantity` int(11) DEFAULT NULL,
  `cancellableQuantity` int(11) DEFAULT NULL,
  `poGeneratedDate` date DEFAULT NULL,
  `rtoDate` date DEFAULT NULL,
  `rtoReason` date DEFAULT NULL,
  `cancelledDate` date DEFAULT NULL,
  `outOfStock` tinyint(4) DEFAULT NULL,
  `freeField1_10` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int(10) UNSIGNED NOT NULL DEFAULT '3' COMMENT '1=admin, 2=management, 3=users',
  `status` int(2) UNSIGNED NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive, 3=block',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `username`, `email`, `email_verified_at`, `password`, `phone`, `address`, `image`, `last_login_at`, `remember_token`, `role`, `status`, `created`, `updated`) VALUES
(1, 'RC', 'admin', 'admin@rcreation.com', NULL, 'WVBuYzRvVDUzdzlxcXdsK2RZbktoZz09', '01877009090', 'Muradpur', 'Jellyfish.jpg', NULL, NULL, 1, 1, '2018-12-31 18:00:00', '2019-01-10 09:37:28'),
(2, 'Shaon', 'shaon76', 'shaon12@gmail.com', NULL, '', '09876', 'ctg', '', NULL, NULL, 2, 1, '2019-01-09 04:34:50', '2019-01-09 10:01:48'),
(3, 'jamal', 'jamal90', 'jamal@gmail.com', NULL, '', '', '', '', NULL, NULL, 3, 1, '2019-01-09 09:13:26', '2019-01-09 09:13:26'),
(4, 'Kamal', 'kamal48', 'kamal@gmail.com', NULL, '', '', '', '', NULL, NULL, 3, 2, '2019-01-09 09:13:53', '2019-01-09 09:13:53');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(10) UNSIGNED NOT NULL,
  `vendorId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendorName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



--
-- Table structure for table `vendors`
--

CREATE TABLE `settings` (
  `settingName` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`countryId`);

--
-- Indexes for table `couriers`
--
ALTER TABLE `couriers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_types`
--
ALTER TABLE `product_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`purchase_id`);

--
-- Indexes for table `purchase_details`
--
ALTER TABLE `purchase_details`
  ADD PRIMARY KEY (`purchase_details_id`);

--
-- Indexes for table `redempation_types`
--
ALTER TABLE `redempation_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `displayName_UNIQUE` (`displayName`);

--
-- Indexes for table `shipments`
--
ALTER TABLE `shipments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `categoryId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `countryId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `couriers`
--
ALTER TABLE `couriers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `purchase_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `purchase_details`
--
ALTER TABLE `purchase_details`
  MODIFY `purchase_details_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `redempation_types`
--
ALTER TABLE `redempation_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shipments`
--
ALTER TABLE `shipments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
